##All the modules.
import os
import sys
import pandas as pd
import datetime as dt
import numpy as np
import warnings
import time
from IPython.display import display
#from fancyimpute import KNN
from DataHandler import *
from Predictor import *
from DBConnectionProvider import *
from sqlalchemy import create_engine
import logging
logging.basicConfig(level=logging.DEBUG)

class DriverForecast(object):

    def executetextinput(self, fpath, cust, dur, interval, outputtype):
        print("executing with text input")
        ##Parsers
        parser = lambda date: pd.datetime.strptime(date, '%d-%m-%Y')
        converter = lambda x: pd.to_numeric(x, 'coerce')

        ##Read the data
        directory0 = os.path.join(fpath, cust, 'input', 'materials')
        if not os.path.exists(directory0):
            print('The directory %s does not exist!!' % directory0)
            sys.exit(0)
        else:
            fnames = list()
            for file in os.listdir(directory0):
                if file.endswith(".txt") or file.endswith(".csv"):
                    fnames.append(os.path.join(directory0, file))

            if len(fnames) < 1:
                sys.exit("Need 1 file but only %d available!!" % len(fnames))
            else:
                fname0 = fnames[0]

        # Read the driver.csv file
        dr = pd.read_csv(fname0, encoding="ISO-8859-1", header=0, parse_dates=[0],
                         date_parser=parser)
        self.driverforecast(fpath, cust, dur, interval, outputtype, dr)

    def executedbinput(self):
        print("executing db input")

    def driverforecast(self,fpath,cust,dur,interval,outputtype,dr):
        print("executing driver forecast");

        cols_dr = list(dr.columns)
        tind = [i for i, s in enumerate(cols_dr) if 'Time'.lower() in s.lower()]
        if len(tind) == 0:
            sys.exit("No time stamp information available!")
        else:
            dr[cols_dr[tind[0]]] = pd.to_datetime(dr[cols_dr[tind[0]]], errors='coerce')
            levels = cols_dr[1:]

        ##Instantiate the classes.
        dh = DataHandler()
        pr = Predict()

        ##Force the driver columns to be numeric and fill gaps gap filler
        ll = []
        for lvl in levels:
            dr[lvl] = pd.to_numeric(dr[lvl], errors='coerce', downcast='float')
            series = dr[lvl].to_frame(name=lvl)
            nanind = list(series[series[lvl].isnull()].index)
            nnanind = list(series[series[lvl].notnull()].index)
            hseries = dh.gapFiller(series, lvl, nanind, nnanind)
            ll.append(hseries)
        df = pd.concat(ll, axis=1)
        df[cols_dr[tind[0]]] = dr[cols_dr[tind[0]]]
        cols = [cols_dr[tind[0]]] + levels
        df = df[cols]

        ##Extract the historical series and create the forecast series template.
        # inp_cols = [cols[tind[0]]] + levels
        edate = pd.to_datetime(df[cols[tind[0]]].values[-1])  ##This is the last time-period in the input series
        fdates = dh.get_fdates(edate, dur, interval)
        ##Form the fseries array:
        hfseries = df.copy()
        hfseries = hfseries.loc[0:dur - 1]
        hfseries.loc[:, cols[tind[0]]] = fdates
        hfseries.loc[:, levels] = np.nan

        # ind_temp = [i for i, s in enumerate(cols) if str(2) in s]
        # cols_temp = [cols[tind[0]], cols_sr[mind[0]], cols_sc[cind[0]]] + [cols[k] for k in ind_temp]
        # display(fseries.loc[:, cols_temp])

        ##Now use the Predict class to predict the time-series forward using the single-series methods.
        dff = pd.DataFrame()
        dffj = pd.DataFrame()
        llp = []
        llj = []
        nlvls = len(levels)
        for l in range(nlvls):
            colsj = ['Method', 'Forecast_Identifier', levels[l] + '_Selected']
            cols_temp = [cols[tind[0]], levels[l]]
            hseries = df.loc[:, cols_temp]
            fseries = hfseries.loc[:, cols_temp]
            stt = time.clock()
            dfp, dfj = pr.forecast_cost(hseries, fseries, levels[l], levels[l], colsj)
            # display(dfp)
            print('Time taken for %s is %.3f' % (levels[l], time.clock() - stt))
            colsj_new = ['Forecasted_Quantity', 'Method', 'Forecast_Identifier', 'Justification']
            dfj = dfj[colsj_new]
            dfp = dfp.reset_index()
            dfp = dfp.drop('index', axis=1)
            if l > 0:
                dfp = dfp.drop(['Time_Period'], axis=1)
            ##Find the method that gives the minimum rmse
            # rmses = dfp[level_str+'_Historical Root-Mean Square Error (RMSE)'].loc[dfp[level_str+'_Historical Root-Mean Square Error (RMSE)'] > 0.0].values
            # # rmses = df_t.loc[:,'Historical Root-Mean Square Error (RMSE)'].values
            # if len(rmses) > 0:
            #     min_rmse = min(rmses)
            #     ind = dfp.loc[dfp[level_str+'_Historical Root-Mean Square Error (RMSE)'] == min_rmse].index
            # else:
            #     print("RMSEs are identical. ")
            #     ind = np.arange(0,dur-1)
            mapes = dfp[levels[l] + '_Mape'].loc[
                dfp[levels[l] + '_Mape'] > 0.0].values
            # rmses = df_t.loc[:,'Historical Root-Mean Square Error (RMSE)'].values
            if len(mapes) > 0:
                min_mape = mapes.min()
                # ind = df_t.loc[df_t['Historical Root-Mean Square Error (RMSE)'] == min_rmse].index
                ind = dfp.loc[dfp[levels[l] + '_Mape'] == min_mape].index
            else:
                # print("RMSEs are identical. ")
                print("MAPEs are identical. ")
                ind = np.arange(0, dur - 1)

            # ind = dfp.loc[dfp['Method'] == 'Simple exponential smoothing (with drift)'].index
            dfp.loc[ind, levels[l] + '_Selected'] = True
            dfp = dfp.loc[dfp[levels[l] + '_Selected'] == True]
            dfp = dfp.rename(columns={'Method': levels[l] + '_Method',
                                      'Forecast_Identifier': levels[l] + '_Forecast_Identifier'})
            # dfj = dfj.rename(columns={'Method': levels[l]+'_Method',
            #                           'Forecast_Identifier': levels[l]+'_Forecast_Identifier',
            #                           'Justification': levels[l]+'_Justification'})
            llp.append(dfp)
            llj.append(dfj)

        for i in range(len(llp)):
            llp[i] = llp[i].reset_index().drop('index', axis=1)
            llj[i] = llj[i].reset_index().drop('index', axis=1)

        # llp[1] = llp[1].drop(cols, axis=1)
        df_temp = pd.concat(llp, axis=1)
        dff = dff.append(pd.concat([df, df_temp]))
        dff = dff.reset_index()
        dff = dff.drop('index', axis=1)

        dffj = dffj.append(pd.concat(llj))
        new_colsj = list(dffj.columns)

        ##Now compute the quarterly averages. First make a copy of the original dataframe containing the predicitons.
        df_t = dff.copy()
        # df = df.drop(df.index[[0, 1]]).reset_index()
        df_t = df_t.reset_index()
        df_t = df_t.drop('index', axis=1)
        df_t = df_t.loc[()]

        ##Convert all future time-periods from str to pandas time-stamps
        df_t[cols_dr[tind[0]]] = pd.to_datetime(df_t[cols_dr[tind[0]]], errors='coerce')
        ##Average to quarterly resolution
        nr = len(df_t.index)
        for lvl in levels:
            i = 0
            while i < nr:
                if df_t[cols_dr[tind[0]]].iloc[i].to_pydatetime().month in (3, 6, 9, 12):
                    if nr - i < 3:
                        j = nr - i
                    else:
                        j = 3
                    df_t.loc[i:i + j, lvl + '_quarterly'] = np.mean(df_t[lvl].values[i:i + j])
                    i = i + 3
                else:
                    i = i + 1

        ##Drop all rows with NaNs in them
        new_cols = list(df_t.columns)
        print(new_cols)
        ind = [i for i, s in enumerate(new_cols) if 'quarterly'.lower() in s.lower()]
        new_lvls = [new_cols[l] for l in ind]
        df_t = df_t[np.isfinite(df_t[new_lvls[0]])]


        ##Write the dataframe to a file.
        if (outputtype == 'text' or outputtype == 'textdb'):
            directory = os.path.join(fpath, cust, 'output', 'materials',
                                     'forecast')  ##Create the directory if it does not exist.
            if not os.path.exists(directory):
                os.makedirs(directory)

        if (outputtype == 'text'):
            ofname0 = os.path.join(directory, 'driver_forecasts.txt')
            ofname0j = os.path.join(directory, 'driver_just_forecasts.txt')
            dh.writeOutput(ofname0, df_t[new_cols])
            dh.writeOutput(ofname0j, dffj[new_colsj])
        elif outputtype == 'db':
            con = DBConnectionProvider().getconnection()
            df_t[new_cols].to_sql(con=con, name="driver_forecasts", if_exists='replace', flavor='mysql', index=False)
            dffj[new_colsj].to_sql(con=con, name="driver_just_forecasts", if_exists='replace', flavor='mysql',
                                   index=False)
            con.close()
        elif outputtype == 'textdb':
            ofname0 = os.path.join(directory, 'driver_forecasts.txt')
            ofname0j = os.path.join(directory, 'driver_just_forecasts.txt')
            dh.writeOutput(ofname0, df_t[new_cols])
            dh.writeOutput(ofname0j, dffj[new_colsj])
            con = create_engine("mysql+pymysql://aqeel:" + 'aqueel@A912D' + "@devdb.cbf1wjhdlwgv.us-east-1.rds.amazonaws.com/hpe26_dev")
            df_t[new_cols].to_sql(con=con,name="driver_forecasts", if_exists='replace', index=False)
            dffj[new_colsj].to_sql(con=con, name="driver_just_forecasts", if_exists='replace',index=False)
            #df_t[new_cols].to_sql(con=con, name="driver_forecasts", if_exists='replace', flavor='mysql', index=False)
            #dffj[new_colsj].to_sql(con=con, name="driver_just_forecasts", if_exists='replace', flavor='mysql',index=False)


if __name__ == '__main__':

    ##Use sysv to get filenames from command line
    fpath    = str(sys.argv[1])
    cust     = str(sys.argv[2])
    dur      = int(sys.argv[3])
    interval = int(sys.argv[4])
    #levels   = sys.argv[5].strip('[]').split(',')

    df = DriverForecast()
    df.executetextinput(fpath, cust, dur, interval, 'textdb')