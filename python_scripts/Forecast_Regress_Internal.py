##All the modules.
import os
import sys
import pandas as pd
import datetime as dt
import numpy as np
import time
from IPython.display import display
import warnings
from DataHandler import *
from Regressor import *
from DBConnectionProvider import *

class ForecastRegressInternal(object):

    def executetextinput(self,fpath,cust,level1,levels,itemsarr,outputtype):
        print("executing text input")
        if (len(itemsarr) > 0):
            print("itemarry is %s " % itemsarr)
            directory0 = os.path.join(fpath, cust, 'output', level1, 'items')
        else:
            directory0 = os.path.join(fpath, cust, 'output', level1)

            ##Get directory path containing relevant files.
        if not os.path.exists(directory0):
            sys.exit('The directory %s does not exist. Please generate the files corresponding to the %s level.' % (
                directory0, level1))
        else:
            fnames = list()
            for file in os.listdir(directory0):
                if file.endswith(".txt"):
                    fnames.append(os.path.join(directory0, file))
            if len(fnames) < 2:
                sys.exit(
                    "The files containing the predictions and the justifications need to be here!! Only %d available!" % len(
                        fnames))
            else:
                fname0j = fnames[0]
                fname0 = fnames[1]
                # fname0 = os.path.join(fpath, cust, 'output', level1, 'predictions_' + level1 + '.txt')  ##This is the MPN level;
                # fnamej = os.path.join(fpath, cust, 'output', level1, 'predictions_just_' + level1 + '.txt')
        level1_str = 'MPN_Unit_Cost'  ##This is the basic level at which forecasts are desired.

        ##Instantiate the classes.
        dh = DataHandler()
        # print(fname0)
        ##Now read the base file containing the predictions.
        df_dep = dh.readFile(fname0, level=level1_str)
        df_depj = pd.read_csv(fname0j, encoding="ISO-8859-1", sep='\t', header=0)  ##This contains the justifications.
        self.forecastregressinternal(fpath, cust, level1, levels, itemsarr, df_dep, df_depj, 'text',outputtype)

    def executedbinput(self,fpath,cust,level1,levels,itemsarr,outputtype):
        print("executing db input")
        #print(level1)
        df_dep = DBConnectionProvider().getdatawithtablename("select * from " + "predictions_" + level1,'predictions_Manufacturer_Part_Number')
        df_depj = DBConnectionProvider().getdatawithtablename("select * from " + "predictions_just_" +level1, 'predictions_just_Manufacturer_Part_Number')
        if(df_depj.empty or df_dep.empty):
            print("predictions or justifications data is not available. Please check")
            sys.exit("predictions or justifications data is not available. Please check")

        self.forecastregressinternal(fpath, cust, level1, levels, itemsarr, df_dep, df_depj,'db',outputtype)


    def forecastregressinternal(self,fpath, cust, level1, levels, itemsarr, df_dep, df_depj, inputtype, outputtype):
        level1_str = 'MPN_Unit_Cost'  ##This is the basic level at which forecasts are desired.
        dh = DataHandler()
        rg = Regressor()
        print("forecast_regress_internal started")
        #print("fpath.. %s " % fpath)

        if df_dep['Time_Period_Size'].iloc[0] == 'QUARTERLY':
            interval = 3
        elif df_dep['Time_Period_Size'].iloc[0] == 'MONTHLY':
            interval = 1
        # edate = pd.to_datetime(df_dep['Data_Identifier'].iloc[0])
        cols = df_dep.columns
        colsj = df_depj.columns
        # display(df_dep)

        ##Get all the mpn_ids
        if len(itemsarr) > 0:
            level_ids = list(df_dep.groupby([level1, 'Manufacturer', 'Customer_Part_Number']).groups.keys())
            new_items = list()
            for item in items:
                for id in level_ids:
                    if item in id[0]:
                        new_items.append(id)
            if len(new_items) == 0:
                sys.exit(
                    'No macth found between item names and level %s. Please supply correct item names that match level specified.' % level1)
            else:
                mpn_ids = new_items
        else:
            mpn_ids = list(df_dep.groupby([level1, 'Manufacturer', 'Customer_Part_Number']).groups.keys())
            #print(mpn_ids)

        # mpn_ids = [('0201B682K100CT', 'Walsin Technology Corporation', '6010B0161001')]
        ##Loop over the mpn_ids
        df_pred = pd.DataFrame()
        df_predj = pd.DataFrame()
        count = 0
        if(inputtype=='db'):
            df_ind_subcommodity = DBConnectionProvider().getdatawithtablename("select * from " + "predictions_subcommodity",'predictions_subcommodity')
            df_ind_commodity = DBConnectionProvider().getdatawithtablename("select * from predictions_commodity",'predictions_commodity')

        for mpn in mpn_ids:
            print(count + 1, mpn)
            if mpn[0] == '_NA':
                print("Skip")
            else:
                ##Get the historical series for given mpn
                #print(mpn)
                series1 = df_dep.groupby([level1, 'Manufacturer', 'Customer_Part_Number']).get_group(mpn)
                seriesj = df_depj.groupby([level1, 'Manufacturer', 'Customer_Part_Number']).get_group(mpn)  ##This is the justifications file
                series1['Time_Period'] = pd.to_datetime(series1['Time_Period'])
                series1 = series1.sort_values(by='Time_Period')
                sdate, edate = dh.get_dates(series1)

                # sdate = series1['Time_Period'].iloc[0]
                dates = list(dh.get_hdates(sdate, interval, edate=edate))

                # edate = dates[-1]
                mask = (series1['Time_Period'] <= edate)

                ##Obtain the input series
                dseries = series1.loc[
                    mask, ['Time_Period', level1_str]]  ##This is just the series containing the MPN_Unit_Cost

                ##Get the output series: This contains the skeleton of the output dataframe that will be populated with the
                ##new predictions from the regression routine.
                fseries = series1.loc[~mask].sort_values(by=['Forecast_Identifier', 'Time_Period'])
                fseries['Selected'] = False
                find = int(fseries['Forecast_Identifier'].drop_duplicates().values[-1])

                ##Now if the series contains only one point, then we cannot perform the regression
                if len(dseries[level1_str].values) <= 1:
                    print("Cannot use linear regression to make predictions for %s" % mpn[0])
                    df_pred = df_pred.append(series1)
                    df_predj = df_predj.append(seriesj)
                else:
                    ##Loop over the different levels
                    df = pd.DataFrame()
                    odf = fseries.drop_duplicates('Time_Period')
                    for lvl in levels:
                        lvl_str = lvl + '_Cost'
                        key_val = series1[lvl].iloc[0]

                        if(inputtype == 'text'):
                            if len(itemsarr) > 0:
                                directory = os.path.join(fpath, cust, 'output', lvl, 'items')
                            else:
                                directory = os.path.join(fpath, cust, 'output', lvl)

                            ##Filename
                            # fname = os.path.join(fpath, cust, 'output', lvl, 'predictions_' + lvl + '.txt')
                            if not os.path.exists(directory):
                                warnings.warn(
                                    'The directory %s does not exist. Please generate the files corresponding to the %s level and place them in %s.' % (
                                    directory, level1, directory))
                            else:
                                fnames = list()
                                for file in os.listdir(directory):
                                    if file.endswith(".txt"):
                                        fnames.append(os.path.join(directory, file))

                                if len(fnames) < 2:
                                    warnings.warn(
                                        "The files containing the predictions and the justifications need to be here!! %d files available. Skipping level %s" % (
                                        len(fnames), lvl))
                                else:
                                    fnamej = fnames[0]
                                    fname = fnames[1]
                                    ##Now read the file corresponding to the appropriate level.
                                    df_ind = dh.readFile(fname, level=lvl_str)
                                    dflvl = df_ind.loc[:, lvl].unique()
                                    if key_val not in dflvl:
                                        warnings.warn('%s is not contained in %s level' % (key_val, lvl))
                                        # warnings.showwarning('%s is not contained in %s level', UserWarning, os.path.join(directory, 'prediction_warnings_'+level1+'_.txt'))
                                    else:
                                        ##Get the series containing the historical values of independent varaible.
                                        iseries = df_ind.loc[
                                            (df_ind['Time_Period'] <= edate) & (df_ind[lvl] == key_val), ['Time_Period',
                                                                                                          lvl_str]]

                                        ##Join the series based on time-period. This will ensure that we have the same number of points for both the
                                        ##predictor and predictand.
                                        hdf = dseries.join(iseries.set_index('Time_Period'), on='Time_Period')

                                        ##Get the future values of the independent variable
                                        fdf = df_ind.loc[(df_ind['Time_Period'] > edate) & (df_ind[lvl] == key_val) & (
                                        df_ind['Selected'] == True), ['Time_Period', lvl_str]]

                                        ##Regress on Sub-Commodity/Commodity
                                        df = df.append(rg.regression(hdf, fdf, odf, level1_str, lvl_str, find + 1),
                                                       ignore_index=True)
                                        find = find + 1
                        else:
                            #print("database")
                            if (lvl == 'SubCommodity'):
                                df_ind = df_ind_subcommodity
                            elif (lvl == 'Commodity'):
                                df_ind = df_ind_commodity

                            if (df_ind.empty):
                                print("dataframe is empty")
                            else:
                                dflvl = df_ind.loc[:, lvl].unique()
                                if key_val not in dflvl:
                                    warnings.warn('%s is not contained in %s level' % (key_val, lvl))
                                    # warnings.showwarning('%s is not contained in %s level', UserWarning, os.path.join(directory, 'prediction_warnings_'+level1+'_.txt'))
                                else:
                                    ##Get the series containing the historical values of independent varaible.
                                    iseries = df_ind.loc[
                                        (df_ind['Time_Period'] <= edate) & (df_ind[lvl] == key_val), ['Time_Period',
                                                                                                      lvl_str]]

                                    ##Join the series based on time-period. This will ensure that we have the same number of points for both the
                                    ##predictor and predictand.
                                    hdf = dseries.join(iseries.set_index('Time_Period'), on='Time_Period')

                                    ##Get the future values of the independent variable
                                    fdf = df_ind.loc[(df_ind['Time_Period'] > edate) & (df_ind[lvl] == key_val) & (df_ind['Selected'] == 'True'), ['Time_Period', lvl_str]]
                                    if (lvl == 'SubCommodity'):
                                        hdf[['MPN_Unit_Cost', 'SubCommodity_Cost']] = hdf[['MPN_Unit_Cost', 'SubCommodity_Cost']].astype(float)
                                        fdf[['SubCommodity_Cost']] = fdf[['SubCommodity_Cost']].astype(float)
                                    elif (lvl == 'Commodity'):
                                        hdf[['MPN_Unit_Cost', 'Commodity_Cost']] = hdf[['MPN_Unit_Cost', 'Commodity_Cost']].astype(float)
                                        fdf[['Commodity_Cost']] = fdf[['Commodity_Cost']].astype(float)

                                    ##Regress on Sub-Commodity/Commodity
                                    df = df.append(rg.regression(hdf, fdf, odf, level1_str, lvl_str, find + 1),
                                                   ignore_index=True)
                                    find = find + 1

                    if df.empty:
                        warnings.warn('Data frame is empty. Not appending.')
                    else:
                        #print(df.columns.values.tolist())
                        df_t = pd.concat([fseries, df], axis=0).reset_index()
                        #df_t[['Mape']] = df_t[['Mape']].astype(float)
                        c = df_t.columns.values.tolist()
                        #print(c)
                        df_t = df_t.drop('index', axis=1)
                        ##Now select the best method based on the minimum rmse
                        # rmses = df_t['Historical Root-Mean Square Error (RMSE)'].loc[df_t['Historical Root-Mean Square Error (RMSE)'] > 0.0].values
                        mapes = df_t['Mape'].loc[
                            df_t['Mape'] > 0.0].values
                        # rmses = df_t.loc[:,'Historical Root-Mean Square Error (RMSE)'].values
                        if len(mapes) > 0:
                            min_mape = mapes.min()
                            # ind = df_t.loc[df_t['Historical Root-Mean Square Error (RMSE)'] == min_rmse].index
                            ind = df_t.loc[df_t['Mape'] == min_mape].index
                        else:
                            # print("RMSEs are identical. ")
                            print("MAPEs are identical. ")
                            ind = np.arange(len(odf.index.values))
                        df_t.loc[ind, 'Selected'] = True
                        fidf = df_t['Forecast_Identifier'].loc[df_t['Selected'] == True].values[0]
                        method = df_t['Method'].loc[df_t['Selected'] == True].values[0]
                        seriesj['Forecast_Identifier'] = fidf
                        seriesj['Method'] = method
                        df_predj = df_predj.append(seriesj)
                        ##Merge the original series with the one containing the predictions
                        df_pred = df_pred.append(pd.concat([series1[mask], df_t], axis=0)).reset_index()
                        df_pred = df_pred.drop('index', axis=1)
            count += 1

        ##Store the file temporarily to a pickle object that can retrieved later.
        # df_pred.to_pickle(os.path.join(directory, 'regress_internal.pkl'))
        # df_predj.to_pickle(os.path.join(directory, 'regress_just_internal.pkl'))
        ##Write output to a file
            ##The output file directory and file names
        if (outputtype == 'text' or outputtype == 'textdb'):
            if len(itemsarr) > 0:
                directory_out = os.path.join(fpath, cust, 'output', level1, 'items', 'item_grouping')
            else:
                directory_out = os.path.join(fpath, cust, 'output',
                                                 'item_grouping')  ##Create the directory if it does not exist.
            if not os.path.exists(directory_out):
                os.makedirs(directory_out)

        if df_pred.empty and df_predj.empty:
            warnings.warn('Dataframe is empty. Nothing to write!!')
        else:
            if (outputtype == 'text'):
                print('writing output to text file')
                ofname0 = os.path.join(directory_out, 'predictions_regress_internal.txt')
                ofname1 = os.path.join(directory_out, 'predictions_just_regress_internal.txt')
                dh.writeOutput(ofname0, df_pred[cols])
                dh.writeOutput(ofname1, df_predj[colsj])
            elif (outputtype == 'db'):
                print('writing output to database')
                con = DBConnectionProvider().getconnection()
                df_pred[cols].to_sql(con=con, name="predictions_regress_internal", if_exists='replace', flavor='mysql', index=False)
                df_predj[colsj].to_sql(con=con, name="predictions_just_regress_internal", if_exists='replace',flavor='mysql',index=False)
            elif (outputtype == 'textdb'):
                print('writing output to text to database')
                ofname0 = os.path.join(directory_out, 'predictions_regress_internal.txt')
                ofname1 = os.path.join(directory_out, 'predictions_just_regress_internal.txt')
                dh.writeOutput(ofname0, df_pred[cols])
                dh.writeOutput(ofname1, df_predj[colsj])

                con = DBConnectionProvider().getconnection()
                df_pred[cols].to_sql(con=con, name="predictions_regress_internal", if_exists='replace', flavor='mysql', index=False)
                df_predj[colsj].to_sql(con=con, name="predictions_just_regress_internal", if_exists='replace',flavor='mysql', index=False)


if __name__ == '__main__':

    ##Use sysv to get filenames from command line
    nargs = len(sys.argv[1:])
    fpath  = str(sys.argv[1])
    cust = str(sys.argv[2])
    level1 = str(sys.argv[3])
    levels = sys.argv[4].strip('[]').split(',')
    fr = ForecastRegressInternal()
    if nargs < 4:
        sys.exit('Need at least 4 arguments.')
    elif nargs == 5:
        items = sys.argv[5].strip('[]').split(',')
        fr.executetextinput(fpath,cust,level1,levels,items)
        #fr.executedbinput(fpath,cust, level1, levels, items)
    else:
        print("4 arguments")
        items = []
        fr.executetextinput(fpath,cust,level1,levels,items,'text')
        #fr.executedbinput(fpath,cust, level1, levels, items,'db')

