##All the modules.
import os
import sys
import pandas as pd
import datetime as dt
import numpy as np
import time
from IPython.display import display
from DataHandler import *
from Regressor import *
import warnings
from DBConnectionProvider import *

class ForecastRegressExternal(object):

    def executetextinput(self, fpath, cust, flag, level1, items, outputtype):
        print("text")

        ##Read in the file containing the materials forecasts
        directory1 = os.path.join(fpath, cust, 'output', 'materials', 'forecast')
        if not os.path.exists(directory1):
            print('The directory %s does not exist!!' % directory1)
            sys.exit(0)
        else:
            fnames = list()
            for file in os.listdir(directory1):
                if file.endswith(".txt") or file.endswith(".csv"):
                    fnames.append(os.path.join(directory1, file))
        if len(fnames) < 1:
            print("No files available. Cannot generate forecasts using material predictors!")
            sys.exit(0)
        else:
            fname1 = fnames[0]

        ##Read in the file containing the MPN-driver mapping
        directory2 = os.path.join(fpath, cust, 'input', 'materials')
        if not os.path.exists(directory2):
            sys.exit('The directory %s does not exist!!' % directory2)
        else:
            fnames = list()
            for file in os.listdir(directory2):
                if file.endswith(".txt") or file.endswith(".csv"):
                    fnames.append(os.path.join(directory2, file))
        if len(fnames) < 1:
            sys.exit(
                "No file containing MPN driver mapping available. Cannot generate forecasts on a per MPN basis using material predictors!")
        else:
            fname2 = fnames[1]

        ##Loop over the levels for which the the predictions are required.
        if flag == '0':  ##Only external
            if len(items) > 0:
                directory0 = os.path.join(fpath, cust, 'output', level1, 'items')
                items = sys.argv[5].strip('[]').split(',')
            else:
                directory0 = os.path.join(fpath, cust, 'output', level1)
            if not os.path.exists(directory0):
                print('The directory %s does not exist!!' % directory0)
                sys.exit(0)
            else:
                fnames = list()
                for file in os.listdir(directory0):
                    if file.endswith(".txt"):
                        fnames.append(os.path.join(directory0, file))
            if len(fnames) < 2:
                print("Need two files here!! Only %d available!" % len(fnames))
                sys.exit(0)
            else:
                fname0j = fnames[0]
                fname0 = fnames[1]

        elif flag == '1':  ##Internal and external
            if len(items) > 0:
                directory0 = os.path.join(fpath, cust, 'output', level1, 'items', 'item_grouping')
                items = sys.argv[5].strip('[]').split(',')
            else:
                directory0 = os.path.join(fpath, cust, 'output', 'item_grouping')
            if not os.path.exists(directory0):
                print('The directory %s does not exist!!' % directory0)
                sys.exit(0)
            else:
                fnames = list()
                for file in os.listdir(directory0):
                    if file.endswith(".txt"):
                        fnames.append(os.path.join(directory0, file))
            if len(fnames) < 2:
                print("Need two files here!! Only %d available!" % len(fnames))
                sys.exit(0)
            else:
                fname0j = fnames[0]
                fname0 = fnames[1]

        ##The variable for which the forecasts need to be generated.
        level1_str = 'MPN_Unit_Cost'  ##This is the basic level at which forecasts are desired.

        ##Instantiate the classes.
        dh = DataHandler()
        rg = Regressor()

        ##Now read the base file containing the MPN forecasts.
        print(fname0)
        df_dep = dh.readFile(fname0, level1_str)
        df_depj = pd.read_csv(fname0j, encoding="ISO-8859-1", sep='\t', header=0)  ##This contains the justifications.

        ##******------Predictor files------*******######
        ##Obtain the column names corresponding to predictors
        # Read the file containing the historical and forecasted time-series of the external predictors
        df_ind = dh.readFile(fname1)
        ##Now read the file containing the MPN-driver mappings for customer 'cust'
        df_ind_mpn = pd.read_csv(fname2, encoding="ISO-8859-1", header=0)
        self.forecastingregressexternal(fpath, cust, level1, items, 'db', df_dep, df_depj, df_ind, df_ind_mpn, outputtype)

    def executedbinput(self):
        print("database")

    def forecastingregressexternal(self, fpath, cust, level1, items, inputtype, df_dep, df_depj, df_ind,df_ind_mpn,outputtype):
        print("executing forrecast_regress_external")
        ##The variable for which the forecasts need to be generated.
        level1_str = 'MPN_Unit_Cost'  ##This is the basic level at which forecasts are desired.

        ##Instantiate the classes.
        dh = DataHandler()
        rg = Regressor()

        if df_dep['Time_Period_Size'].iloc[0] == 'QUARTERLY':
            interval = 3
        elif df_dep['Time_Period_Size'].iloc[0] == 'MONTHLY':
            interval = 1
        # edate = pd.to_datetime(df_dep['Data Identifier'].iloc[0])
        cols = df_dep.columns
        colsj = df_depj.columns

        ##Get all the mpn_ids
        if len(items) > 0:
            level_ids = list(df_dep.groupby([level1, 'Manufacturer', 'Customer_Part_Number']).groups.keys())
            new_items = list()
            for item in items:
                for id in level_ids:
                    if item in id[0]:
                        new_items.append(id)
            if len(new_items) == 0:
                sys.exit(
                    'No macth found between item names and level %s. Please supply correct item names that match level specified.' % level1)
            else:
                mpn_ids = new_items
        else:
            mpn_ids = list(df_dep.groupby([level1, 'Manufacturer', 'Customer_Part_Number']).groups.keys())
            # mpn_ids = [('6050A2551901', 'Gold Circuit Electronics Ltd.', '6050A2551901')]

        cols_dr = list(df_ind.columns)
        levels = [s.split('_')[0] for i, s in enumerate(cols_dr) if 'quarterly'.lower() in s.lower()]
        lvl_strs = [s + '_quarterly' for s in levels]
        tind = [i for i, s in enumerate(cols_dr) if 'Time'.lower() in s.lower()]
        if len(tind) == 0:
            sys.exit(
                "No time stamp information available. Please include a column named 'Time_Period' in the input file which contains time stamps")
        else:
            df_ind[cols_dr[tind[0]]] = pd.to_datetime(df_ind[cols_dr[tind[0]]], errors='coerce')
        df_ind['Time_Period'] = df_ind.loc[:, cols_dr[tind[0]]]
        # df_ind = df_ind.drop(cols_dr[tind[0]], axis=1)

        ##Apply conversion to numeric values to lvl_strs
        for c in lvl_strs:
            df_ind[c] = pd.to_numeric(df_ind[c], errors='coerce')


        cols_mpn = list(df_ind_mpn.columns)
        ##MPN column name
        mind = [i for i, s in enumerate(cols_mpn) if 'mpn'.lower() in s.lower()]
        if len(mind) > 0:
            mpn_col = cols_mpn[mind[0]]
        else:
            sys.exit(
                'No MPNs. Please supply the mapping between MPNs and relevant driver names in this file')

        ##Factor name
        find = [i for i, s in enumerate(cols_mpn) if 'factor'.lower() in s.lower()]
        if len(find) > 0:
            factor_col = cols_mpn[find[0]]
        else:
            sys.exit(
                'No factor names . Please supply the mapping between MPNs and relevant driver names in this file')


        # display(df_ind_mpn.head(20))
        ##Loop over the mpn_ids
        df_pred = pd.DataFrame()
        df_predj = pd.DataFrame()
        count = 0
        for mpn in mpn_ids:
            print(mpn[0])
            ##Get the driver columns corresponding to the mpn
            levels = list(df_ind_mpn.loc[(df_ind_mpn[mpn_col] == mpn[0])][factor_col])
            if len(levels) == 0:
                print(
                    'No drivers available for this %s. Cannot perform regression against exogeneous drivers. Skipping!' %
                    mpn[0])
                series1 = df_dep.groupby([level1, 'Manufacturer', 'Customer_Part_Number']).get_group(mpn)
                seriesj = df_depj.groupby([level1, 'Manufacturer', 'Customer_Part_Number']).get_group(mpn)
                df_pred = df_pred.append(series1)
                df_predj = df_predj.append(seriesj)
                print(count + 1)
            else:
                print(count + 1)
                lvl_strs = [s + '_quarterly' for s in levels]
                ##Get the historical series for given mpn
                series1 = df_dep.groupby([level1, 'Manufacturer', 'Customer_Part_Number']).get_group(mpn)
                seriesj = df_depj.groupby([level1, 'Manufacturer', 'Customer_Part_Number']).get_group(mpn)
                series1['Time_Period'] = pd.to_datetime(series1['Time_Period'])
                series1 = series1.sort_values(by='Time_Period')
                sdate, edate = dh.get_dates(series1)
                # sdate = series1['Time_Period'].iloc[0]
                dates = list(dh.get_hdates(sdate, interval, edate=edate))
                # edate = dates[-1]
                mask = (series1['Time_Period'] <= edate)

                ##Obtain the input series
                dseries = series1.loc[
                    mask, ['Time_Period', level1_str]]  ##This is just the series containing the MPN_Unit_Cost

                ##Get the output series: This contains the skeleton of the output dataframe that will be populated with the
                ##new predictions from the regression routine.
                fseries = series1.loc[~mask].sort_values(by=['Forecast_Identifier', 'Time_Period'])
                fseries['Selected'] = False
                find = int(fseries['Forecast_Identifier'].drop_duplicates().values[-1])
                ##Now if the series contains only one point, then we cannot perform the regression
                if len(dseries[level1_str].values) <= 1:
                    print("Cannot use linear regression to make predictions for %s" % mpn[0])
                    df_pred = df_pred.append(series1)
                    df_predj = df_predj.append(seriesj)
                else:
                    ##Loop over the different levels
                    df = pd.DataFrame()
                    odf = fseries.drop_duplicates('Time_Period')

                    ##Get the series containing the historical values of independent varaible.
                    cols_new = [cols_dr[tind[0]]] + lvl_strs
                    iseries = df_ind.loc[(df_ind[cols_dr[tind[0]]] <= edate), cols_new]

                    ##Join the series based on time-period. This will ensure that we have the same number of points for both the
                    ##predictor and predictand.
                    hdf = dseries.join(iseries.set_index('Time_Period'), on='Time_Period')

                    ####Get the future values of the independent variable. Would need to match time-stamps here
                    fdf = odf['Time_Period'].to_frame().join(df_ind.set_index('Time_Period'), on='Time_Period')[cols_new]

                    ##Regress on predictors
                    df = df.append(rg.regression(hdf, fdf, odf, level1_str, lvl_strs, find + 1), ignore_index=True)

                    df_t = pd.concat([fseries, df], axis=0).reset_index()
                    df_t = df_t.drop('index', axis=1)
                    ##Now select the best method based on the minimum rmse
                    # rmses = df_t['Historical_RMSE'].loc[df_t['Historical_RMSE'] > 0.0].values
                    # # rmses = df_t.loc[:,'Historical_RMSE'].values
                    # if len(rmses) > 0:
                    #     min_rmse = min(rmses)
                    #     ind = df_t.loc[df_t['Historical_RMSE'] == min_rmse].index
                    # else:
                    #     print("RMSEs are identical. ")
                    #     ind = np.arange(len(odf.index.values))

                    mapes = df_t['Mape'].loc[df_t['Mape'] > 1E-10].values
                    # rmses = df_t.loc[:,'Historical_RMSE'].values
                    if len(mapes) > 0:
                        min_mape = mapes.min()
                        # ind = df_t.loc[df_t['Historical_RMSE'] == min_rmse].index
                        ind = df_t.loc[df_t['Mape'] == min_mape].index
                    else:
                        # print("RMSEs are identical. ")
                        print("MAPEs are identical. ")
                        ind = np.arange(len(odf.index.values))
                    df_t.loc[ind, 'Selected'] = True
                    fidf = df_t['Forecast_Identifier'].loc[df_t['Selected'] == True].values[0]
                    # method = df_t['Method'].loc[df_t['Selected'] == True].values[0]
                    seriesj['Forecast_Identifier'] = fidf
                    seriesj['Method'] = 'Regression on material predictors'
                    df_predj = df_predj.append(seriesj)
                    ##Merge the original series with the one containing the predictions
                    df_pred = df_pred.append(pd.concat([series1[mask], df_t], axis=0)).reset_index()
                    df_pred = df_pred.drop('index', axis=1)

            count += 1

        ##******The output file path******
        ##Write output to a file
        if (outputtype == 'text' or outputtype == 'textdb'):
            if len(items) > 0:
                directory_out = os.path.join(fpath, cust, 'output', level1, 'items',
                                                 'materials')  ##Create the directory if it does not exist.
            else:
                directory_out = os.path.join(fpath, cust, 'output',
                                                 'materials')  ##Create the directory if it does not exist.
            if not os.path.exists(directory_out):
                os.makedirs(directory_out)

        ##Write output to file or db
        if (outputtype == 'text'):
            print("writing output to text file")
            if df_pred.empty:
                warnings.warn('Data frame is empty. No drivers found for any MPN in the dataframe!')
            else:
                ofname0 = os.path.join(directory_out, 'predictions_regress_on_materials.txt')
                ofname1 = os.path.join(directory_out, 'predictions_just_regress_on_materials.txt')
                dh.writeOutput(ofname0, df_pred[cols])
                dh.writeOutput(ofname1, df_predj[colsj])
        elif(outputtype == 'db'):
            print("writing output to database")
            con = DBConnectionProvider().getconnection()
            print("writing to database")
            df_pred[cols].to_sql(con=con, name="predictions_regress_on_materials", if_exists='replace', flavor='mysql',
                                 index=False)
            df_predj[colsj].to_sql(con=con, name="predictions_just_regress_on_materials", if_exists='replace',
                                   flavor='mysql', index=False)
        elif(outputtype == 'textdb'):
            print("writing output to text file")
            if df_pred.empty:
                warnings.warn('Data frame is empty. No drivers found for any MPN in the dataframe!')
            else:
                ofname0 = os.path.join(directory_out, 'predictions_regress_on_materials.txt')
                ofname1 = os.path.join(directory_out, 'predictions_just_regress_on_materials.txt')
                dh.writeOutput(ofname0, df_pred[cols])
                dh.writeOutput(ofname1, df_predj[colsj])

            con = DBConnectionProvider().getconnection()
            print("writing to database")
            df_pred[cols].to_sql(con=con, name="predictions_regress_on_materials", if_exists='replace', flavor='mysql',
                                 index=False)
            df_predj[colsj].to_sql(con=con, name="predictions_just_regress_on_materials", if_exists='replace',
                                   flavor='mysql', index=False)



if __name__ == '__main__':

    ##Use sysv to get filenames from command line
    nargs = len(sys.argv[1:])
    fpath = str(sys.argv[1])
    cust  = str(sys.argv[2])
    flag  = str(sys.argv[3])
    level1 = str(sys.argv[4])
    #levels = sys.argv[5].strip('[]').split(',')
    items = []
    if nargs == 6:
        items = sys.argv[6].strip('[]').split(',')

    frext = ForecastRegressExternal()
    frext.executetextinput(fpath, cust, flag, level1, items, 'text')


