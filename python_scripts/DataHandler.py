##All the module definitions
import pandas as pd
import datetime as dt
import numpy as np
import csv
from sys import exit
from IPython.display import display
import time
import sys

class DataHandler(object):
    """
    Contains functions to read all files and return data frames, generate regularized time-stamps and future forecast intervals
    """
    ## Define the lambda function called parser that converts date string to a datetime object.
    ##We also define a converter function that ensures that cost data are indeed numeric while any
    ##non-numeric fields are converted into a floating point nan number.
    def __init__(self):
        "This iniitializes methods that will be used across all methods and attributes."
        self.parser = lambda date: pd.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
        self.parser1 = lambda date: pd.datetime.strptime(date, '%Y-%m-%d')
        self.parser2 = lambda date: pd.datetime.strptime(date, '%d-%m-%Y')
        self.converter = lambda x: pd.to_numeric(x, 'coerce')


    def readData(self, customerHistory, customerPartClassification, customerDataInfo):
        ##+
        ##Name: readData
        ##Purpose:
        #    To read data from three tab delimited text files and return a single dataframe containing
        ##  merged information from all theree files
        ##Input:
        ##  customerHistory, customerPartClassification, customerDataInfo -- Full filepath of files containing
        ##  customer specific information such as CPN, MPN, Manufacturer, MPN Cost etc.
        ##Output :
        ## df -- Dataframe containing merged information from all three files.
        ##Author and creation date:
        ## Aditya Murthi, 06/16/17
        ##-

        ##Customer history file read
        cust = pd.read_csv(customerHistory, encoding="ISO-8859-1", sep='\t', header=0, parse_dates=[5],
                           date_parser=self.parser, converters={'MPN_Unit_Cost': self.converter})
        ##Part Classification file read
        part = pd.read_csv(customerPartClassification, sep = '\t', encoding = "ISO-8859-1", header=0)

        ##Customer Data particulars file read
        sys_info = pd.read_csv(customerDataInfo, sep = '\t', encoding = "ISO-8859-1", header=0)

        date_str = sys_info['Current_Date'].values[0]
        data_id = dt.datetime.strptime(date_str, '%Y-%m-%d').strftime('%Y-%m-%d')

        ##Merge the three dataframes containing the data.
        keys = ['Customer_Part_Number', 'Manufacturer_Part_Number', 'Manufacturer']

        self.data = cust.join(part.set_index(keys), on=keys, how='inner')
        self.data['Data_Identifier'] = data_id
        self.data = self.data[self.icols]

        return self.data

    def readFile(self, fname, level=None):
        """This method is used to read a generic file."""
        try:
            df = pd.read_csv(fname, encoding="ISO-8859-1", sep='\t', header=0, parse_dates=[0],
                    date_parser=self.parser, converters={level: self.converter})
        except(ValueError):
            try:
                df = pd.read_csv(fname, encoding="ISO-8859-1", sep='\t', header=0, parse_dates=[0],
                    date_parser=self.parser1, converters={level: self.converter})
            except(ValueError):
                df =  pd.read_csv(fname, encoding="ISO-8859-1", sep='\t', header=0)

        return df

    #Method to aggregate cost data at level specified.
    def aggregator(self, data, level):
        """This function aggregates the cost data at the desired level. Right now
        forecasts are generated at the MPN_ID level but they can be aggregated by
        Sub-Commodity and Commodity.
        level: Here level is a string which is NOT 'Manufacturer_Part_Number' which
        is the finest level of granularity. You can aggregate up from this level."""

        if level == 'Manufacturer_Part_Number':
            #np = data['Manufacturer_Part_Number'].nunique()
            level_ids = list(data.groupby(['Manufacturer_Part_Number']).groups.keys())
            return data, level_ids
        else:
            level_ids = list(data.groupby([level]).groups.keys())
            if len(level_ids) == 1:##Commodity level
                df = data.groupby([level]).get_group(lkeys[0])
                df[level + '_Cost'] = df['MPN_Unit_Cost'].groupby(df['Time_Period']).transform('sum')
                df = df.drop_duplicates(['Time_Period'])
                del df['MPN_Unit_Cost']
            else:##Sub-Commodity level
                df = pd.DataFrame()
                for k in level_ids:
                    series = data.groupby([level]).get_group(k)
                    series[level + '_Cost'] = series['MPN_Unit_Cost'].groupby(series['Time_Period']).transform('sum')
                    series[level + '_Quantity'] = series['Quantity'].groupby(series['Time_Period']).transform('sum')
                    series = series.drop_duplicates(['Time_Period'])
                    del series['MPN_Unit_Cost']
                    del series['Quantity']
                    df = df.append(series,ignore_index=True)

            return df, level_ids

    ##Method to add months
    def add_months(self, sourcedate, months):
        month = sourcedate.month - 1 + months
        year = int(sourcedate.year + month / 12)
        month = month % 12 + 1
        day = 1
        return dt.date(year, month, day)

    ##Generate of series with historical dates. This is used to get the end date.
    def get_hdates(self, sdate, interval, edate=None):
        if edate == None:
            curr_date = dt.datetime.now()
            first = curr_date.replace(day=1)
            edate = first - dt.timedelta(days=1)
            #edate = edate.replace(day=1)
        temp = sdate.date()
        dates = list()
        dates.append(pd.to_datetime(sdate))
        while temp < edate.date():
            tdate = self.add_months(temp, interval)
            temp = tdate
            if temp <= edate.date():
                dates.append(temp)
            # print(temp, curr_date.date())

        #del dates[-1]
        # print(edate.strftime('%Y-%m-%d %H:%M:%S'))
        return pd.to_datetime(pd.Series(dates))

    ##Now given my end date, I want to generate time-stamps for n quarters
    def get_fdates(self, edate, dur, interval):
        fdates = list()
        temp = edate
        for i in range(dur):
            dd = self.add_months(temp, interval)
            fdates.append(dd.strftime('%Y-%m-%d %H:%M:%S'))
            temp = dd
        return fdates

        ##Function to get the start and end date of the historical time_period.
    def get_dates(self, df, sdate=None, edate=None):
        """This function is used to get the start and end dates from the dataframe provided."""
        ##Get the start date of the historical period.
        if sdate is None:
            cols = list(df.columns)
            ind = [i for i, s in enumerate(cols) if 'Start'.lower() in s.lower()]
            if len(ind) > 0:
                sdate = pd.to_datetime(df[cols[ind[0]]].values[0])
            else:
                sdate = pd.to_datetime(df['Time_Period'].values[0])
        else:
            sdate = sdate
        ##This is the end date of the historical period.
        if edate is None:
            cols = list(df.columns)
            ind = [i for i, s in enumerate(cols) if 'Data_Identifier' in s.lower()]
            if len(ind) > 0:
                edate = pd.to_datetime(df[cols[ind[0]]].values[0])
            else:
                try:
                    edate = pd.to_datetime(df['Data_Identifier'].iloc[0])
                except(KeyError):
                    print('Key missing from dataframe')
                    edate = None
        else:
            edate = edate

        return sdate, edate

    ##Data Preparation method:
    def series_generator(self, series, level_str, q_str, end_date, dur, interval):

        """Takes in a series and generates uniformly (quarterly) distributed time stamps along with
        the corresponding meta data. The actual numeric values for these missing time-stamps are set to NaN."""
        cols = series.columns
        nh = len(series.index.values)
        ###Then only one data point available. fseries is not avaialble. Can only forecast using Random walk.
        sdate = pd.to_datetime(series['Time_Period'].values[0])
        dates = list(self.get_hdates(sdate, interval, edate=end_date))
        nd = len(dates)
        edate = dates[-1]
        series['Time_Period'] = pd.to_datetime(series['Time_Period'])
        if nh == 1:
            hseries_new = series
            hseries_new['Time_Period'].iloc[0] = edate
            ind = hseries_new.index.values
            ##Now generate forecast dates
            fdates = self.get_fdates(edate, dur, interval)
            fseries = pd.DataFrame(columns=hseries_new.columns)
            index = [ind[0] + i for i in range(1, dur + 1)]
            for i in range(dur):
                fseries.loc[i] = hseries_new.loc[ind[0]]
                fseries['Time_Period'].iloc[i] = pd.to_datetime(fdates[i])
            if level_str != 'MPN_Unit_Cost':
                fseries[level_str] = np.nan
                fseries[q_str] = np.nan
            fseries['index'] = index
            fseries = fseries.set_index(['index'], drop=True)
        else:
            series = series.sort_values(by='Time_Period')
            mask = (series['Time_Period'] >= sdate) & (series['Time_Period'] <= edate)
            hseries = series.loc[mask]
            mpn_list = list(hseries.groupby(['Manufacturer_Part_Number', 'Manufacturer', 'Customer_Part_Number']).groups.keys())
            df_s = pd.DataFrame(columns=hseries.columns)
            #mpn_list = [('0201YC102KAT2A', 'AVX Corporation', '201-0049')]
            for mpn in mpn_list:
                #print(mpn)
                tseries = hseries.groupby(['Manufacturer_Part_Number', 'Manufacturer', 'Customer_Part_Number']).get_group(mpn)#.drop_duplicates('Time_Period')
                #display(tseries[['Time_Period','Manufacturer_Part_Number', 'Manufacturer', 'Customer_Part_Number', 'CM_or_ODM', 'MPN_Unit_Cost', 'Quantity']])
                cms = list(tseries.groupby(['CM_or_ODM']).groups.keys())
                if len(cms) > 1:
                    #print('Averaging across all CM/ODMs')
                    ll = list()
                    for cm in range(len(cms)):
                        h1 = tseries.groupby(['CM_or_ODM']).get_group(cms[cm]).drop_duplicates('Time_Period')
                        ind = h1.index.values
                        sind = ind[0]
                        if len(h1.index.values) < nd:
                            ##Have to generate data for some missing time-stamps
                            tdates = pd.to_datetime(h1['Time_Period'].values)
                            mask_fill = list()
                            for d in dates:
                                if d in tdates:
                                    mask_fill.append(True)
                                else:
                                    mask_fill.append(False)

                            indexes = [sind + i for i in range(len(mask_fill))]
                            indv = [indx for indx, x in enumerate(mask_fill) if mask_fill[indx] == False]
                            hist = pd.DataFrame(columns=h1.columns)
                            x = 0
                            for i in range(len(mask_fill)):
                                if i in indv:
                                    hist.loc[i] = h1.loc[ind[0]]
                                    hist['Time_Period'].iloc[i] = dates[i]
                                    hist['MPN_Unit_Cost'].iloc[i] = np.nan
                                    hist['Quantity'].iloc[i] = np.nan
                                else:
                                    hist.loc[i] = h1.loc[ind[x]]
                                    x += 1
                            hist['index'] = indexes
                            hist = hist.set_index(['index'], drop=True)
                            ll.append(hist)
                        else:
                            ll.append(h1)

                    ##Create a new df
                    hseries_new = pd.concat(ll, axis=0)
                    hseries_new = hseries_new.sort_values(by='Time_Period')
                    hseries_new['new_index'] = [hseries_new.index.values[0] + i for i in range(len(hseries_new.index.values))]
                    hseries_new = hseries_new.set_index(['new_index'], drop=True)
                    #display(hseries_new[['Time_Period','Manufacturer_Part_Number', 'Manufacturer', 'Customer_Part_Number', 'CM_or_ODM', 'MPN_Unit_Cost', 'Quantity']])
                    #sys.exit(0)
                    ##Compute the quantity weighted average
                    hseries_new = hseries_new.sort_values(by=['CM_or_ODM', 'Time_Period'])
                    g = hseries_new.groupby(['Time_Period'])
                    hseries_new['wa'] = (hseries_new['MPN_Unit_Cost'] / g['Quantity'].transform("sum")) * hseries_new['Quantity']
                    hseries_new['MPN_Unit_Cost'] = g.wa.transform("sum")
                    hseries_new = hseries_new[cols].drop_duplicates('Time_Period')
                    #display(hseries_new[['Time_Period', 'Manufacturer_Part_Number', 'Manufacturer', 'Customer_Part_Number','CM_or_ODM', 'MPN_Unit_Cost', 'Quantity']])
                    #sys.exit(0)
                else:
                    ind = tseries.index.values
                    sind = ind[0]
                    if len(ind) < nd:
                        ##Have to generate data for some missing time-stamps
                        tdates = pd.to_datetime(tseries['Time_Period'].values)
                        mask_fill = list()
                        for d in dates:
                            if d in tdates:
                                mask_fill.append(True)
                            else:
                                mask_fill.append(False)

                        indexes = [sind + i for i in range(len(mask_fill))]
                        indv = [indx for indx, x in enumerate(mask_fill) if mask_fill[indx] == False]
                        hseries_new = pd.DataFrame(columns=tseries.columns)
                        x = 0
                        for i in range(len(mask_fill)):
                            if i in indv:
                                hseries_new.loc[i] = hseries.loc[ind[0]]
                                hseries_new['Time_Period'].iloc[i] = dates[i]
                                hseries_new['MPN_Unit_Cost'].iloc[i] = np.nan
                                hseries_new['Quantity'].iloc[i] = np.nan
                            else:
                                hseries_new.loc[i] = tseries.loc[ind[x]]
                                x += 1
                        hseries_new['index'] = indexes
                        hseries_new = hseries_new.set_index(['index'], drop=True)

                    else:
                        hseries_new = tseries

                df_s = df_s.append(hseries_new)

            df_s = df_s.reset_index()
            df_s = df_s.drop('index', axis=1)

            ##If the dataframe contains Infs then replace with Nan.
            df_s = df_s.replace([np.inf, -np.inf], np.nan)
            ##Now get the forecast dates.
            ind = df_s.index.values
            fdates = self.get_fdates(edate, dur, interval)
            index = [df_s.index.values[-1] + i for i in range(1, dur + 1)]
            fseries = pd.DataFrame(columns=df_s.columns)
            for i in range(dur):
                fseries.loc[i] = df_s.loc[ind[0]]
                fseries['Time_Period'].iloc[i] = pd.to_datetime(fdates[i])

            if level_str != 'MPN_Unit_Cost':
                fseries[level_str] = np.nan
                fseries[q_str] = np.nan
            fseries['index'] = index
            fseries = fseries.set_index(['index'], drop=True)
            hseries_new = df_s[cols]

        return hseries_new, fseries

    ##Data Preparation method:
    def series_generator_comm(self, series, levels, sdate, dur, interval, edate=None):

        """Takes in a series and generates uniformly (quarterly) distributed time stamps along with
        the corresponding meta data. The actual numeric values for these missing time-stamps are set to NaN."""
        cols = series.columns
        nh = len(series.index.values)
        lvl_str1 = levels[1]
        lvl_str2 = levels[2]
        ###Then only one data point available. fseries is not avaialble. Can only forecast using Random walk.
        #sdate = series['Time_Period'].iloc[0]
        if edate is None:
            dates = list(self.get_hdates(sdate, interval))
            edate = dates[-1]
        else:
            dates = list(self.get_hdates(sdate, interval, edate=edate))

        nd = len(dates)
        series['Time_Period'] = pd.to_datetime(series['Time_Period'])
        if nh == 1:
            hseries_new = series
            hseries_new['Time_Period'].iloc[0] = edate
            ind = hseries_new.index.values
            ##Now generate forecast dates
            fdates = self.get_fdates(edate, dur, interval)
            fseries = pd.DataFrame(columns=hseries_new.columns)
            index = [ind[0] + i for i in range(1, dur + 1)]
            for i in range(dur):
                fseries.loc[i] = hseries_new.loc[ind[0]]
                fseries['Time_Period'].iloc[i] = pd.to_datetime(fdates[i])
            fseries['index'] = index
            fseries = fseries.set_index(['index'], drop=True)
        else:
            series = series.sort_values(by='Time_Period')
            #display(series)
            mask = (series['Time_Period'] >= sdate) & (series['Time_Period'] <= edate)
            hseries = series.loc[mask]
            ind = hseries.index.values
            sind = ind[0]
            if len(ind) < nd:
                ##Have to generate data for some missing time-stamps
                tdates = pd.to_datetime(hseries['Time_Period'].values)
                mask_fill = list()
                for d in dates:
                    if d in tdates:
                        mask_fill.append(True)
                    else:
                        mask_fill.append(False)

                indexes = [sind + i for i in range(len(mask_fill))]
                indv = [indx for indx, x in enumerate(mask_fill) if mask_fill[indx] == False]
                hseries_new = pd.DataFrame(columns=hseries.columns)
                x = 0
                for i in range(len(mask_fill)):
                    if i in indv:
                        hseries_new.loc[i] = hseries.loc[ind[0]]
                        hseries_new['Time_Period'].iloc[i] = dates[i]
                        hseries_new[lvl_str1].iloc[i] = np.nan
                        hseries_new[lvl_str2].iloc[i] = np.nan
                    else:
                        hseries_new.loc[i] = hseries.loc[ind[x]]
                        x += 1
                hseries_new['index'] = indexes
                hseries_new = hseries_new.set_index(['index'], drop=True)
            else:
                hseries_new = hseries

            ##Now get the forecast dates.
            hseries_new = hseries_new.replace([np.inf, -np.inf], np.nan)
            ind = hseries_new.index.values
            fdates = self.get_fdates(edate, dur, interval)
            index = [hseries_new.index.values[-1] + i for i in range(1, dur + 1)]
            fseries = pd.DataFrame(columns=hseries_new.columns)
            for i in range(dur):
                fseries.loc[i] = hseries_new.loc[ind[0]]
                fseries['Time_Period'].iloc[i] = pd.to_datetime(fdates[i])

            fseries['index'] = index
            fseries = fseries.set_index(['index'], drop=True)
            hseries_new = hseries_new[cols]
            fseries = fseries[cols]

        return hseries_new, fseries

    def gapFiller(self, hseries, lvl, nanind, nnanind):

        """This method takes in a series and fills the NaN values in the MPN_Unit_Cost column using the random walk method.
        Need to add interpolation based techniques as well based on simple and moving (window) avergaes as well."""
        # indexes = hseries.index.values  ##The original indexes
        # ##Reset index temporarily to facilitate gap filling.
        # hseries = hseries.reset_index()
        ni = len(nanind)
        if ni == 1:
            if nanind[0] == 0:
                hseries[lvl].iloc[nanind[0]] = hseries[lvl].iloc[nanind[0] + 1]
            else:
                hseries[lvl].iloc[nanind[0]] = hseries[lvl].iloc[nanind[0] - 1]
        else:
            for i in range(ni):
                if nanind[i] == 0:
                    if nanind[i + 1] != 1:
                        hseries[lvl].iloc[nanind[i]] = hseries[lvl].iloc[nanind[i] + 1]
                    else:
                        hseries[lvl].iloc[nanind[i]] = hseries[lvl].iloc[nnanind[i]]
                else:
                    hseries[lvl].iloc[nanind[i]] = hseries[lvl].iloc[nanind[i] - 1]

        ##Reset the index to the original values.
        #hseries['index'] = indexes
        #hseries = hseries.set_index(['index'], drop=True)
        return hseries


    def writeOutput(self, fname, data):
         """This function writes output to a csv file. Actually, this might not be required since this is just a one liner."""
         data.to_csv(fname, sep='\t', na_rep=np.float('nan'), float_format='%.12f', index=False)

         return None

    # def qualityCheckInputData(self, inputDataDirectory):
    #     """
    #     Checks if the input data is correct. We keep adding all the errors we find in the input data so that it can be corrected at source over time.
    #     Things checked include but may not be limited to:
    #     0. Number of files and their names. If they are not correct, print correct names and exit.
    #     1. Column names and their sequence in each of the input files. If incorrect, print correct names and exit.
    #     2. Ordering of dates (provide as a warning - saying that results may not be correct).
    #     3. Unacceptable values in lieu of NaN - values like hyphen etc. are not allowed.
    #     4. Missing values in the fields - print warnings.
    #     5. Number of fields in each record should match the number of columns. If they don't, print warnings with row numbers for each of them, with expected and actual counts.
    #     6. Summarize the data in each column of each file - counts for all data, count of unique values for all non-numerics, statistics for all numerics
    #     7. Consistency of the period lengths with what is stated in the System Information file.
    #     :param data:
    #     :return:
    #     """

