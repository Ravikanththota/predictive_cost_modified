import pymysql
import pandas as pd
import numpy as np


class DBConnectionProvider(object):
    def getconnection(self):
        print("Trying to get connection")
        db = pymysql.connect("devdb.cbf1wjhdlwgv.us-east-1.rds.amazonaws.com", "aqeel", "aqueel@A912D", "hpe26_dev")
        #db = pymysql.connect("devdb.cbf1wjhdlwgv.us-east-1.rds.amazonaws.com", "aqeel", "aqueel@A912D", "levadata26_dev")
        #db = pymysql.connect("testdb.cbf1wjhdlwgv.us-east-1.rds.amazonaws.com", "devuser", "Password890","trimble_rfq2")
        #db = pymysql.connect("172.16.0.107", "root", "root", "predictive_cost_dev")
        return db

    def getdata(self,query):
        db = DBConnectionProvider().getconnection()
        cursor = db.cursor()
        cursor.execute(query)
        names = [x[0] for x in cursor.description]
        print(names)
        rows = cursor.fetchall()
        dd = cursor.rowcount
        if (dd > 0):
            df = pd.DataFrame(np.array(rows), columns=names)
        else:
            df = pd.DataFrame
        return df

    def getdatawithtablename(self,query,tablename):
        result = self.checkTables(tablename)
        if(result):
            db = DBConnectionProvider().getconnection()
            cursor = db.cursor()
            cursor.execute(query)
            names = [x[0] for x in cursor.description]
            print(names)
            rows = cursor.fetchall()
            dd = cursor.rowcount
            if(dd>0):
                df = pd.DataFrame(np.array(rows), columns=names)
            else:
                df = pd.DataFrame
            return df
        else:
            print("Table with name %s does not exists " % tablename)
            df = pd.DataFrame
            return df

    def checkTables(self,tablename):
        stmt = "SHOW TABLES LIKE '%s' " % ('%' + str(tablename) + '%')
        db = DBConnectionProvider().getconnection()
        cursor = db.cursor()
        cursor.execute(stmt)
        result = cursor.fetchone()
        return result



if __name__ == '__main__':
    print("test dbconnection provider")
