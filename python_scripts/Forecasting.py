from IPython.display import display
import sys
import os
from abc import ABCMeta
import pandas as pd
import datetime as dt
import numpy as np
import time
import statsmodels.formula.api as smf
import scipy.stats as stats
from sklearn.metrics import mean_squared_error
from statsmodels.sandbox.regression.predstd import wls_prediction_std
from patsy import dmatrix
from statsmodels.tsa.arima_model import ARIMA
from numpy.linalg.linalg import LinAlgError as linalgerr
from DBConnectionProvider import *
from PredictiveCostConstant import *


class Forecast(object):
    """This is the main forecasting class that every other class inherits basic properties from.
    """
    __metaclass__ = ABCMeta

    def __init__(self):
        self.parser = lambda date: pd.datetime.strptime(date, '%Y-%m-%d')
        self.converter = lambda x: pd.to_numeric(x, 'coerce')
        self.icols = ['Time_Period', 'Customer_ID', 'Data_Identifier', 'Time_Period_Size', 'Customer_Part_Number',
                      'Manufacturer_Part_Number', 'Manufacturer', 'CM_or_ODM', 'Commodity',
                      'SubCommodity', 'LevaData_SubCommodity', 'Quantity', 'MPN_Unit_Cost']
        self.cols_ext = ['Method', 'Forecast_Identifier', 'Historical_RMSE',
                         'Mape', 'Standard_Error', 'CI_Lower',
                         'CI_Upper']
        self.cols_output = self.icols + self.cols_ext
        ##Columns required for the Justification table.
        self.cols_just = ['Customer_ID', 'Data_Identifier', 'Customer_Part_Number', 'Manufacturer_Part_Number',
                          'Manufacturer', 'Commodity', 'SubCommodity', 'LevaData_SubCommodity', 'Method',
                          'Forecast_Identifier', 'Selected']
        self.nan = np.float64('nan')

    def executetextinput(self, fpath, cust, levels, periods, items, outputtype):
        print("executing text input")
        ifpath = os.path.join(fpath, cust,'input')  ##This is the full filepath of the directory containing the three input files.
        # levels   = sys.argv[3].strip('[]').split(',')
        ##Strings to print.
        print("Full filepath of directory containing the three files: %s" % ifpath)
        print("\n")
        print("The number of periods for which forecasts is desired is %d" % int(periods))

        ##STEP1: Get the filenames corresponding to the customer data. Read the data into dataframes.
        fnames = list()
        for file in os.listdir(ifpath):
            if file.endswith(".txt"):
                fnames.append(os.path.join(ifpath, file))
                # print(os.path.join(fpath, file))

        custHistory, custPartClass, custDataInfo = fnames[0], fnames[1], fnames[2]

        ##Read in Customer, Part Classification and System Information data.
        dh = DataHandler()
        pr = Predict()
        df = dh.readData(custHistory, custPartClass, custDataInfo)
        self.forecasting(fpath, cust, levels, periods, items, df,outputtype)

    def executedbinput(self, fpath, cust, levels, periods, items,outputtype):
        # Read from database
        customer = DBConnectionProvider().getdata(customerData)
        customer[['MPN_Unit_Cost', 'Quantity']] = customer[['MPN_Unit_Cost', 'Quantity']].astype(float)
        part = DBConnectionProvider().getdata(partClassificationData)
        sys_info = DBConnectionProvider().getdata(systemInformation)
        print("Data Retrieved from database")
        date_str = str(sys_info['Current_Date'].values[0])
        data_id = dt.datetime.strptime(date_str, '%Y-%m-%d').strftime('%Y-%m-%d')

        ##Merge the three dataframes containing the data.
        keys = ['Customer_Part_Number', 'Manufacturer_Part_Number', 'Manufacturer']

        data = customer.join(part.set_index(keys), on=keys, how='inner')
        data['Data_Identifier'] = data_id
        data['Time_Period_Size'] = sys_info['Time_Period_Size'].iloc[0]
        data = data[self.icols]
        dh = DataHandler()
        self.forecasting(fpath, cust, levels, periods, items, data,outputtype)

    def forecasting(self, fpath, cust, levels, periods, items, df,outputtype):
        dh = DataHandler()
        pr = Predict()
        if df['Time_Period_Size'].iloc[0] == 'QUARTERLY':
            interval = 3
        elif df['Time_Period_Size'].iloc[0] == 'MONTHLY':
            interval = 1

        print("................ " + str(levels))
        #for levell in levels:
            #print("++++++++++ " + levell)
        ##Loop through levels
        for level in levels:
            print("The predictions are generated at the %s level" % level)
            ##Group data by the appropriate level at which the predictions are desired.
            if level == 'Manufacturer_Part_Number':
                level_str = 'MPN_Unit_Cost'
                q_str = 'Quantity'
                level_ids = list(df.groupby([level, 'Manufacturer', 'Customer_Part_Number']).groups.keys())
                #print("length of inditems %s " % items)
                if (len(items)) > 0:
                    new_items = list()
                    for item in items:
                        for id in level_ids:
                            if item in id[0]:
                                new_items.append(id)
                    if len(new_items) == 0:
                        sys.exit(
                            'No macth found between item names and level %s. Please supply correct item names that match level specified.' % level)
                    else:
                        level_ids = new_items
                ##The columns utilized for the justification df
                cols_just = ['Customer_ID', 'Data_Identifier', 'Time_Period_Size', 'Customer_Part_Number',
                             'Manufacturer',
                             level]
                colsj = cols_just + ['Method', 'Forecast_Identifier', 'Selected']
            else:
                level_str = level + '_Cost'
                q_str = level + '_Quantity'
                level_ids = list(df.groupby([level]).groups.keys())

                if len(items) > 0:
                    new_items = list()
                    for item in items:
                        for id in level_ids:
                            if item in id:
                                new_items.append(id)
                    if len(new_items) == 0:
                        sys.exit(
                            'No macth found between item names and level %s. Please supply correct item names that match level specified.' % level)
                    else:
                        level_ids = new_items

                ##The columns utilized for the justification df
                cols_just = ['Customer_ID', 'Data_Identifier', 'Time_Period_Size', level]
                colsj = cols_just + ['Method', 'Forecast_Identifier', 'Selected']

            ##STEP2: Loop over these level_ids to generate forecasts.
            count = 0
            c0 = 0
            # ##Create two empty dataframes to store the predictions and the justifications for the predictions.
            df_pred = pd.DataFrame()
            df_just = pd.DataFrame()
            start = time.clock()

            # level_ids = [('0201YC102KAT2A', 'AVX Corporation', '201-0049')]
            # level_ids = ['_NA']
            for id in level_ids:
                print(count, id, c0)
                if level == 'Manufacturer_Part_Number':
                    idd = id[0]
                else:
                    idd = id
                if idd == '_NA':
                    print("Skip")
                else:
                    if level == 'Manufacturer_Part_Number':
                        series = df.groupby([level, 'Manufacturer', 'Customer_Part_Number']).get_group(id)
                    else:
                        series = df.groupby([level]).get_group(id)

                    series['Time_Period'] = pd.to_datetime(series['Time_Period'])
                    series = series.sort_values(by='Time_Period')

                    ##The number of points (original) in the series
                    npoints = len(series.drop_duplicates('Time_Period').index.values)

                    ##The start date has to be smaller than the curr_date - 1
                    sdate, edate = dh.get_dates(series)

                    # sdate = pd.to_datetime(series['Time_Period'].values[0])
                    ##Get the end date corresponding to the historical period.
                    # edate = pd.to_datetime(df['Data_Identifier'].iloc[0])
                    # curr_date = dt.datetime.now()
                    # first = curr_date.replace(day=1)
                    # lastMonth = first - dt.timedelta(days=1)
                    # lastMonth = lastMonth.replace(day=1)
                    if sdate.date() > edate.date():
                        print("The start date %s is > %s" % (sdate.strftime('%Y-%m-%d'), edate.strftime('%Y-%m-%d')))
                        print("Skip")
                    else:
                        ##STEP3: If the forecast is desried at a particular level, then aggregate the MPN_Cost data to this level.
                        ##NOTE: There are different ways to "aggregate". One is to compute the simple sum of all MPN_Unit_Cost values
                        ##irrespective of quantity. The other way is to compute the quantity weighted average value (normalize w.r.t quantity).
                        ##Right now, we're just computing the simple arithematic sum.
                        if level == 'Manufacturer_Part_Number':
                            new_series = series
                            hseries, fseries = dh.series_generator(new_series, level_str, q_str, edate, periods,
                                                                   interval)
                            colst = ['Time_Period'] + cols_just + [level_str, 'SubCommodity', 'Commodity',
                                                                   'LevaData_SubCommodity']
                            hseries = hseries[colst]
                            fseries = fseries[colst]
                        else:
                            ##STEP4: This should generate the series with appropriate time-stamps generated in.
                            hseries, fseries = dh.series_generator(series, level_str, q_str, edate, periods, interval)
                            ##STEP 5: Aggregate to desired level
                            hseries = dh.aggregator(hseries, level)
                            colst = ['Time_Period'] + cols_just + [level_str]
                            hseries = hseries[colst]
                            fseries = fseries[colst]

                        ##STEP 4: Before the series is supplied to the forecasting function for the purpose of making predictions, we need to fill
                        ##gaps whereever possible.
                        indexes = hseries.index.values  ##The original indexes
                        ##Reset index temporarily to facilitate gap filling.
                        hseries = hseries.reset_index()
                        nanind = list(hseries[hseries[level_str].isnull()].index)
                        nnanind = list(hseries[hseries[level_str].notnull()].index)

                        if len(nnanind) == 0:
                            npoints = len(nnanind)
                            df_temp = hseries[cols_just].iloc[
                                0].to_frame().transpose()  # .drop(['Time_Period', level_str, q_str], axis=1)
                            df_temp['Method'] = 'NaN'
                            df_temp['Forecast_Identifier'] = 'NaN'
                            df_temp['Selected'] = 'NaN'
                            df_temp['Forecasted_Level'] = level
                            df_temp['Forecasted_Quantity'] = level_str
                            df_temp['Justification'] = 'No MPN Unit cost data available to make any forecasts.'
                            df_temp['Numbe_of_data_points'] = npoints
                            df_just = df_just.append(df_temp)
                            c0 += 1
                        elif len(nnanind) == len(hseries.index.values):
                            ##1. Calculate the forecast using each method
                            dfp, dfj = pr.forecast_cost(hseries, fseries, level, level_str, colsj)
                            df_t = pd.concat([hseries, dfp], axis=0)
                            df_t['Forecasted_Level'] = level
                            df_t['Forecasted_Quantity'] = level_str
                            df_pred = df_pred.append(df_t)
                            dfj['Numbe_of_data_points'] = npoints
                            df_just = df_just.append(dfj)
                        else:
                            ##There are gaps that need to be filled. This uses the simple random walk to fill gaps.
                            hseries_new = dh.gapFiller(hseries, level_str, nanind, nnanind)

                            ##1. Calculate the forecast using each method
                            dfp, dfj = pr.forecast_cost(hseries_new, fseries, level, level_str, colsj)
                            df_t = pd.concat([hseries_new, dfp], axis=0)
                            df_t['Forecasted_Level'] = level
                            df_t['Forecasted_Quantity'] = level_str
                            df_pred = df_pred.append(df_t)
                            dfj['Numbe_of_data_points'] = npoints
                            df_just = df_just.append(dfj)

                count += 1

            ##Now parse the data and generate t
            ##Write output to a file.
            if level == 'Manufacturer_Part_Number':
                cols = ['Time_Period'] + cols_just + [level_str, 'SubCommodity', 'Commodity', 'LevaData_SubCommodity',
                                                      'Forecasted_Level', 'Forecasted_Quantity'] \
                       + ['Method', 'Forecast_Identifier', 'Historical_RMSE',
                          'Mape', 'Standard_Error', 'CI_Lower',
                          'CI_Upper', 'Selected']
            else:
                cols = ['Time_Period'] + cols_just + [level_str, 'Forecasted_Level', 'Forecasted_Quantity'] \
                       + ['Method', 'Forecast_Identifier', 'Historical_RMSE',
                          'Mape', 'Standard_Error', 'CI_Lower',
                          'CI_Upper', 'Selected']

            if(outputtype == 'text' or 'textdb'):
                if len(items) > 0:
                    directory = os.path.join(fpath, cust, 'output', level,
                                             'items')  ##Create the directory if it does not exist.
                    if not os.path.exists(directory):
                        os.makedirs(directory)
                else:
                    directory = os.path.join(fpath, cust, 'output', level)  ##Create the directory if it does not exist.
                    if not os.path.exists(directory):
                        os.makedirs(directory)

            if df_pred.empty and df_just.empty:
                print('Dataframe is empty. Nothing to write!!')
            else:
                # ##Files to write predictions to: There are two files here, the first one is called predictions and the second one is called predictions_just.txt
                if(outputtype=='text'):
                    print("writing outptut to text file")
                    ofname0 = os.path.join(directory, 'predictions_' + level + '.txt')
                    ofname1 = os.path.join(directory, 'predictions_just_' + level + '.txt')

                    dh.writeOutput(ofname0, df_pred[cols])
                    dh.writeOutput(ofname1, df_just)
                elif(outputtype == 'db'):
                    print("writing output to database")
                    con = DBConnectionProvider().getconnection()
                    df_pred[cols].to_sql(con=con, name="predictions_" + level, if_exists='replace', flavor='mysql',
                                         index=False)
                    df_just.to_sql(con=con, name="predictions_just_" + level, if_exists='replace', flavor='mysql',
                                   index=False)
                elif(outputtype == 'textdb'):
                    print("writing output to text and database")
                    ofname0 = os.path.join(directory, 'predictions_' + level + '.txt')
                    ofname1 = os.path.join(directory, 'predictions_just_' + level + '.txt')
                    dh.writeOutput(ofname0, df_pred[cols])
                    dh.writeOutput(ofname1, df_just)
                    con = DBConnectionProvider().getconnection()
                    df_pred[cols].to_sql(con=con, name="predictions_" + level, if_exists='replace', flavor='mysql',index=False)
                    df_just.to_sql(con=con, name="predictions_just_" + level, if_exists='replace', flavor='mysql',index=False)

        print(time.clock() - start)


##This is the datahandler class which inherits from the Forecast class.

class DataHandler(Forecast):
    """
    Contains functions to read all files and return data frames, generate regularized time-stamps and future forecast intervals
    """

    def readData(self, customerHistory, customerPartClassification, customerDataInfo):
        ##+
        ##Name: readData
        ##Purpose:
        #    To read data from three tab delimited text files and return a single dataframe containing
        ##  merged information from all theree files
        ##Input:
        ##  customerHistory, customerPartClassification, customerDataInfo -- Full filepath of files containing
        ##  customer specific information such as CPN, MPN, Manufacturer, MPN Cost etc.
        ##Output :
        ## df -- Dataframe containing merged information from all three files.
        ##Author and creation date:
        ## Aditya Murthi, 06/16/17
        ##-

        ##Customer history file read
        cust = pd.read_csv(customerHistory, encoding="ISO-8859-1", sep='\t', header=0, parse_dates=[5],
                           date_parser=self.parser, converters={'MPN_Unit_Cost': self.converter})
        ##Part Classification file read
        part = pd.read_csv(customerPartClassification, sep='\t', encoding="ISO-8859-1", header=0)
               ##Customer Data particulars file read
        sys_info = pd.read_csv(customerDataInfo, sep='\t', encoding="ISO-8859-1", header=0)

        date_str = sys_info['Current_Date'].values[0]
        data_id = dt.datetime.strptime(date_str, '%Y-%m-%d').strftime('%Y-%m-%d')

        ##Merge the three dataframes containing the data.
        keys = ['Customer_Part_Number', 'Manufacturer_Part_Number', 'Manufacturer']

        self.data = cust.join(part.set_index(keys), on=keys, how='inner')
        self.data['Data_Identifier'] = data_id
        self.data['Time_Period_Size'] = sys_info['Time_Period_Size'].iloc[0]
        self.data = self.data[self.icols]

        return self.data

    # Method to aggregate cost data at level specified.
    def aggregator(self, series, level):
        """This function aggregates the cost data at the desired level. Right now
        forecasts are generated at the MPN_ID level but they can be aggregated by
        Sub-Commodity and Commodity.
        level: Here level is a string which is NOT 'Manufacturer_Part_Number' which
        is the finest level of granularity. You can aggregate up from this level."""

        ##For the historical series
        ##The first step is aggregating across all CM/ODMs
        series[level + '_Cost'] = series['MPN_Unit_Cost'].groupby(series['Time_Period']).transform('sum')
        series[level + '_Quantity'] = series['Quantity'].groupby(series['Time_Period']).transform('sum')
        aseries = series.sort_values(by='Time_Period').drop_duplicates(['Time_Period']).drop(
            ['Manufacturer_Part_Number', 'MPN_Unit_Cost', 'Quantity'], axis=1)
        if level == 'Commodity':
            aseries = aseries.drop('SubCommodity', axis=1)
            aseries = aseries.drop('LevaData_SubCommodity', axis=1)

        return aseries

    ##Method to add months
    def add_months(self, sourcedate, months):
        month = sourcedate.month - 1 + months
        year = int(sourcedate.year + month / 12)
        month = month % 12 + 1
        day = 1
        return dt.date(year, month, day)

    ##Generate of series with historical dates. This is used to get the end date.
    def get_hdates(self, sdate, interval, edate=None):
        if edate == None:
            curr_date = dt.datetime.now()
            first = curr_date.replace(day=1)
            edate = first - dt.timedelta(days=1)
            # edate = edate.replace(day=1)
        temp = sdate.date()
        dates = list()
        dates.append(pd.to_datetime(sdate))
        while temp < edate.date():
            tdate = self.add_months(temp, interval)
            temp = tdate
            if temp < edate.date():
                dates.append(temp)
                # print(temp, curr_date.date())
        # del dates[-1]
        # print(edate.strftime('%Y-%m-%d %H:%M:%S'))
        return pd.to_datetime(pd.Series(dates))

    ##Now given my end date, I want to generate time-stamps for n quarters
    def get_fdates(self, edate, dur, interval):
        fdates = list()
        temp = edate
        for i in range(dur):
            dd = self.add_months(temp, interval)
            fdates.append(dd.strftime('%Y-%m-%d'))
            temp = dd
        return fdates

    ##Function to get the start and end date of the historical time_period.
    def get_dates(self, df, sdate=None, edate=None):
        """This function is used to get the start and end dates from the dataframe provided."""
        ##Get the start date of the historical period.
        if sdate is None:
            cols = list(df.columns)
            ind = [i for i, s in enumerate(cols) if 'Start'.lower() in s.lower()]
            if len(ind) > 0:
                sdate = pd.to_datetime(df[cols[ind[0]]].values[0])
            else:
                sdate = pd.to_datetime(df['Time_Period'].values[0])
        else:
            sdate = sdate
        ##This is the end date of the historical period.
        if edate is None:
            cols = list(df.columns)
            ind = [i for i, s in enumerate(cols) if 'Data_Identifier' in s.lower()]
            if len(ind) > 0:
                edate = pd.to_datetime(df[cols[ind[0]]].values[0])
            else:
                edate = pd.to_datetime(df['Data_Identifier'].iloc[0])
        else:
            edate = edate

        return sdate, edate

    ##Data Preparation method:
    def series_generator(self, series, level_str, q_str, end_date, dur, interval):

        """Takes in a series and generates uniformly (quarterly) distributed time stamps along with
        the corresponding meta data. The actual numeric values for these missing time-stamps are set to NaN."""
        cols = series.columns
        nh = len(series.index.values)
        ###Then only one data point available. fseries is not avaialble. Can only forecast using Random walk.
        sdate = pd.to_datetime(series['Time_Period'].values[0])
        dates = list(self.get_hdates(sdate, interval, edate=end_date))
        nd = len(dates)
        edate = dates[-1]
        series['Time_Period'] = pd.to_datetime(series['Time_Period'])
        if nh == 1:
            hseries_new = series
            hseries_new['Time_Period'].iloc[0] = edate
            ind = hseries_new.index.values
            ##Now generate forecast dates
            fdates = self.get_fdates(edate, dur, interval)
            fseries = pd.DataFrame(columns=hseries_new.columns)
            index = [ind[0] + i for i in range(1, dur + 1)]
            for i in range(dur):
                fseries.loc[i] = hseries_new.loc[ind[0]]
                fseries['Time_Period'].iloc[i] = pd.to_datetime(fdates[i])
            if level_str != 'MPN_Unit_Cost':
                fseries[level_str] = np.nan
                fseries[q_str] = np.nan
            fseries['index'] = index
            fseries = fseries.set_index(['index'], drop=True)
        else:
            series = series.sort_values(by='Time_Period')
            mask = (series['Time_Period'] >= sdate) & (series['Time_Period'] <= edate)
            hseries = series.loc[mask]
            mpn_list = list(
                hseries.groupby(['Manufacturer_Part_Number', 'Manufacturer', 'Customer_Part_Number']).groups.keys())
            print("length of mpns ... %s " % len(mpn_list))
            df_s = pd.DataFrame(columns=hseries.columns)
            # mpn_list = [('0201YC102KAT2A', 'AVX Corporation', '201-0049')]
            for mpn in mpn_list:
                # print(mpn)
                tseries = hseries.groupby(
                    ['Manufacturer_Part_Number', 'Manufacturer', 'Customer_Part_Number']).get_group(
                    mpn)  # .drop_duplicates('Time_Period')
                # display(tseries[['Time_Period','Manufacturer_Part_Number', 'Manufacturer', 'Customer_Part_Number', 'CM_or_ODM', 'MPN_Unit_Cost', 'Quantity']])
                cms = list(tseries.groupby(['CM_or_ODM']).groups.keys())
                if len(cms) > 1:
                    # print('Averaging across all CM/ODMs')
                    ll = list()
                    for cm in range(len(cms)):
                        h1 = tseries.groupby(['CM_or_ODM']).get_group(cms[cm]).drop_duplicates('Time_Period')
                        ind = h1.index.values
                        sind = ind[0]
                        if len(h1.index.values) < nd:
                            ##Have to generate data for some missing time-stamps
                            tdates = pd.to_datetime(h1['Time_Period'].values)
                            mask_fill = list()
                            for d in dates:
                                if d in tdates:
                                    mask_fill.append(True)
                                else:
                                    mask_fill.append(False)

                            indexes = [sind + i for i in range(len(mask_fill))]
                            indv = [indx for indx, x in enumerate(mask_fill) if mask_fill[indx] == False]
                            hist = pd.DataFrame(columns=h1.columns)
                            x = 0
                            for i in range(len(mask_fill)):
                                if i in indv:
                                    hist.loc[i] = h1.loc[ind[0]]
                                    hist['Time_Period'].iloc[i] = dates[i]
                                    hist['MPN_Unit_Cost'].iloc[i] = np.nan
                                    hist['Quantity'].iloc[i] = np.nan
                                else:
                                    hist.loc[i] = h1.loc[ind[x]]
                                    x += 1
                            hist['index'] = indexes
                            hist = hist.set_index(['index'], drop=True)
                            ll.append(hist)
                        else:
                            ll.append(h1)

                    ##Create a new df
                    hseries_new = pd.concat(ll, axis=0)
                    hseries_new = hseries_new.sort_values(by='Time_Period')
                    hseries_new['new_index'] = [hseries_new.index.values[0] + i for i in
                                                range(len(hseries_new.index.values))]
                    hseries_new = hseries_new.set_index(['new_index'], drop=True)
                    # display(hseries_new[['Time_Period','Manufacturer_Part_Number', 'Manufacturer', 'Customer_Part_Number', 'CM_or_ODM', 'MPN_Unit_Cost', 'Quantity']])
                    # sys.exit(0)
                    ##Compute the quantity weighted average
                    hseries_new = hseries_new.sort_values(by=['CM_or_ODM', 'Time_Period'])
                    g = hseries_new.groupby(['Time_Period'])
                    hseries_new['wa'] = (hseries_new['MPN_Unit_Cost'] / g['Quantity'].transform("sum")) * hseries_new[
                        'Quantity']
                    hseries_new['MPN_Unit_Cost'] = g.wa.transform("sum")
                    hseries_new = hseries_new[cols].drop_duplicates('Time_Period')
                    # display(hseries_new[['Time_Period', 'Manufacturer_Part_Number', 'Manufacturer', 'Customer_Part_Number','CM_or_ODM', 'MPN_Unit_Cost', 'Quantity']])
                    # sys.exit(0)
                else:
                    ind = tseries.index.values
                    sind = ind[0]
                    if len(ind) < nd:
                        ##Have to generate data for some missing time-stamps
                        tdates = pd.to_datetime(tseries['Time_Period'].values)
                        mask_fill = list()
                        for d in dates:
                            if d in tdates:
                                mask_fill.append(True)
                            else:
                                mask_fill.append(False)

                        indexes = [sind + i for i in range(len(mask_fill))]
                        indv = [indx for indx, x in enumerate(mask_fill) if mask_fill[indx] == False]
                        hseries_new = pd.DataFrame(columns=tseries.columns)
                        x = 0
                        for i in range(len(mask_fill)):
                            if i in indv:
                                hseries_new.loc[i] = hseries.loc[ind[0]]
                                hseries_new['Time_Period'].iloc[i] = dates[i]
                                hseries_new['MPN_Unit_Cost'].iloc[i] = np.nan
                                hseries_new['Quantity'].iloc[i] = np.nan
                            else:
                                hseries_new.loc[i] = tseries.loc[ind[x]]
                                x += 1
                        hseries_new['index'] = indexes
                        hseries_new = hseries_new.set_index(['index'], drop=True)

                    else:
                        hseries_new = tseries

                df_s = df_s.append(hseries_new)

            df_s = df_s.reset_index()
            df_s = df_s.drop('index', axis=1)

            ##If the dataframe contains Infs then replace with Nan.
            df_s = df_s.replace([np.inf, -np.inf], np.nan)
            ##Now get the forecast dates.
            ind = df_s.index.values
            fdates = self.get_fdates(edate, dur, interval)
            index = [df_s.index.values[-1] + i for i in range(1, dur + 1)]
            fseries = pd.DataFrame(columns=df_s.columns)
            for i in range(dur):
                fseries.loc[i] = df_s.loc[ind[0]]
                fseries['Time_Period'].iloc[i] = pd.to_datetime(fdates[i])

            if level_str != 'MPN_Unit_Cost':
                fseries[level_str] = np.nan
                fseries[q_str] = np.nan
            fseries['index'] = index
            fseries = fseries.set_index(['index'], drop=True)
            hseries_new = df_s[cols]

        return hseries_new, fseries

    def gapFiller(self, hseries, level_str, nanind, nnanind):

        """This method takes in a series and fills the NaN values in the MPN_Unit_Cost column using the random walk method.
        Need to add interpolation based techniques as well based on simple and moving (window) avergaes as well."""
        indexes = hseries.index.values  ##The original indexes
        ##Reset index temporarily to facilitate gap filling.
        hseries = hseries.reset_index()
        ni = len(nanind)
        if ni == 1:
            if nanind[0] == 0:
                hseries[level_str].iloc[nanind[0]] = hseries[level_str].iloc[nanind[0] + 1]
            else:
                hseries[level_str].iloc[nanind[0]] = hseries[level_str].iloc[nanind[0] - 1]
        else:
            for i in range(ni):
                if nanind[i] == 0:
                    if nanind[i + 1] != 1:
                        hseries[level_str].iloc[nanind[i]] = hseries[level_str].iloc[nanind[i] + 1]
                    else:
                        hseries[level_str].iloc[nanind[i]] = hseries[level_str].iloc[nnanind[i]]
                else:
                    hseries[level_str].iloc[nanind[i]] = hseries[level_str].iloc[nanind[i] - 1]

        ##Reset the index to the original values.
        hseries['index'] = indexes
        hseries = hseries.set_index(['index'], drop=True)
        return hseries

    def writeOutput(self, fname, data):
        """This function writes output to a csv file. Actually, this might not be required since this is just a one liner."""
        data.to_csv(fname, sep='\t', na_rep=np.float('nan'), float_format='%.5f', index=False, mode='w')
        return None


##The predictor class
class Predict(Forecast):
    ##Forecast error metrics
    def forecast_accuracy(self, test, pred):

        '''This function calculates several error metrics to evaluate forecast accuracy'''
        # mse = mean_squared_error(test, pred)
        # print("The mean square error using the historical mean to predict observed data is %3f" % mse)
        ##Standard error = square root of the mean squared error or root-mean square error
        rmse = mean_squared_error(test, pred) ** 0.5

        # print("The standard or root mean square error using the historical mean to predict observed data is %3f" % rmse)
        ##MAPE (Mean Absolute Percentage Error)
        if all(i > 0.0 for i in test):
            err = np.array(pred) - np.array(test)
            mape = (np.mean(np.abs(np.asarray(err) / np.asarray(test)))) * 100
            # print("The mean absolute percentage error using the historical mean to predict observed data is %3f%%" % mape)
        else:
            mape = 'NaN'

        return rmse, mape

        ##1. The simple mean model

    def simple_average(self, hseries, fseries, level_str):
        """Simple historical average: Calculate the simple average of all the historical observations and use that
           as an estimate of the predicted cost for all future periods."""

        method_name = 'Mean Model'
        hcost = hseries[level_str].values
        n = len(hcost)
        cost = hcost
        ##Compute the historical average
        avg = cost.mean()
        pred = np.array([avg for _ in range(len(cost))])
        rmse, mape = self.forecast_accuracy(cost, pred)

        ##Calculate standard error and confidence intervals
        err = pred - hcost
        sd_err = np.std(err)  ##Model standard error
        fserr = np.sqrt((sd_err) ** 2 + (sd_err) ** 2 / len(hcost))  ##Forecast standard error (1-step)
        ##CIs: The CI's for the mean model do not grow in time as the forecast horizon
        t_critical = stats.t.ppf(q=0.975,
                                 df=n - 1)  ##This is used instead of the z-value since the population std dev is not known
        lb = avg - t_critical * fserr
        if lb < 0.0:
            lb = 0.0
        ub = avg + t_critical * fserr

        ##Create a dataframe with the predicitons
        df0 = pd.DataFrame()
        df0 = df0.append(fseries)
        i = 0
        for index, row in df0.iterrows():
            df0[level_str].iloc[i] = avg
            i += 1

        df0['Method'] = method_name
        df0['Forecast_Identifier'] = '1'
        df0['Historical_RMSE'] = rmse
        df0['Mape'] = mape
        df0['Standard_Error'] = fserr
        df0['CI_Lower'] = lb
        df0['CI_Upper'] = ub

        # ##Create an empty dataframe
        # df0 = pd.DataFrame(columns=self.cols_output)
        # ##Generate forecast for fdates: The forecast in this case is just the value of the historical mean
        # for index, row in fseries.iterrows():
        #     temp_dict = {}
        #     temp = [str(row[self.cols_output[x]]) for x in range(len(self.cols_output[0:9]))]
        #     temp.append(avg)
        #     temp.append(method_name)
        #     temp.append('1')
        #     temp.append(rmse)
        #     temp.append(mape)
        #     temp.append(fserr)
        #     temp.append(lb)
        #     temp.append(ub)
        #     for c in range(len(self.cols_output)):
        #         temp_dict[self.cols_output[c]] = temp[c]
        #     df0.loc[index] = pd.Series(temp_dict)

        return df0, rmse

        ##2. Random walk

    def random_walk(self, hseries, fseries, level_str):
        """Random-walk: This method is based on the assumption that the past observations provide NO
        information on the future direction (trend) of the series. The RW with drift is
        just like the random-walk but with an additive drift component that corresponds
        to the average period-to-period change observed in the past. This drift component pertains to the
        mean step-size."""

        method_name = 'Random walk'
        hcost = hseries[level_str].values
        fdates = fseries['Time_Period'].values
        nf = len(fdates)
        n = len(hcost)

        ##Make out-of sample forecasts
        sd_err = np.std(np.diff(hcost))

        ##Forecast from the last observed point out.
        t_c = stats.t.ppf(q=0.975, df=n - 2)
        forecast = list()
        lb = list()
        ub = list()
        rmserr = list()
        for k in range(nf):
            forecast.append(hcost[n - 1])
            l = forecast[k] - t_c * sd_err * np.sqrt(k + 1)
            u = forecast[k] + t_c * sd_err * np.sqrt(k + 1)
            if l < 0:
                l = 0.0
            rmserr.append(sd_err * np.sqrt(k + 1))
            lb.append(l)
            ub.append(u)

        if n == 1:
            ##If there is only one-point available, then rmse/mape is not defined.
            rmse = 0.0
            mape = 0.0

        else:
            ##Estimates of the forecast accuracy
            history = [hcost[0]]
            test = [hcost[i] for i in range(1, len(hcost))]
            pred = list()
            for k in range(len(test)):
                yhat = history[k]
                obs = test[k]
                pred.append(yhat)
                history.append(obs)
                # print('predicted=%f, expected=%f' % (yhat, obs))

            ##Prediction Error
            rmse, mape = self.forecast_accuracy(test, pred)

        ##Create a dataframe with the predicitons
        df = pd.DataFrame()
        df = df.append(fseries)
        i = 0
        for index, row in df.iterrows():
            df[level_str].iloc[i] = forecast[i]
            i += 1
        df['Method'] = method_name
        df['Forecast_Identifier'] = '2'
        df['Historical_RMSE'] = rmse
        df['Mape'] = mape
        df['Standard_Error'] = rmserr
        df['CI_Lower'] = lb
        df['CI_Upper'] = ub

        # ##Create an empty dataframe
        # df = pd.DataFrame(columns=self.cols_output)
        # ##Generate forecast for fdates:
        # i = 0
        # for index, row in fseries.iterrows():
        #     temp_dict = {}
        #     temp = [str(row[self.cols_output[x]]) for x in range(len(self.cols_output[0:9]))]
        #     temp.append(forecast[i])
        #     temp.append(method_name)
        #     temp.append('2')
        #     temp.append(rmse)
        #     temp.append(mape)
        #     temp.append(rmserr[i])
        #     temp.append(lb[i])
        #     temp.append(ub[i])
        #     for c in range(len(self.cols_output)):
        #         temp_dict[self.cols_output[c]] = temp[c]
        #     df.loc[index] = pd.Series(temp_dict)
        #     i = i + 1

        return df, rmse

    ##3. Random-walk with drift
    def random_walk_drift(self, hseries, fseries, level_str):
        """Random-walk with drift: The RW with drift is just like the random-walk but
        with an additive drift component that corresponds to the average period-to-period
        change observed in the past. This drift component pertains to the mean step-size."""

        method_name = 'Random walk with drift'
        hcost = np.array(hseries[level_str].values)
        fdates = fseries['Time_Period'].values
        nf = len(fdates)
        n = len(hcost)
        ##Estimate the drift term
        d = (hcost[n - 1] - hcost[0]) / (n - 1)

        ##Estimate the standard error of the k-step ahead forecast
        sd_err = np.std(np.diff(hcost))

        ##Forecast from the last observed point out.
        t_c = stats.t.ppf(q=0.975, df=n - 2)
        forecast = list()
        lb = list()
        ub = list()
        rmserr = list()
        for k in range(nf):
            tcost = hcost[-1] + (k + 1) * d
            # print(k, hcost[-1], tcost, min(hcost[-1], np.abs(tcost)))
            if tcost < 0.0:
                ##Reflect the value
                forecast.append(min(hcost[-1], np.abs(tcost)))
            else:
                forecast.append(tcost)

            l = forecast[k] - t_c * sd_err * np.sqrt(k + 1)
            u = forecast[k] + t_c * sd_err * np.sqrt(k + 1)
            if l < 0:
                l = 0.0
            rmserr.append(sd_err * np.sqrt(k + 1))
            lb.append(l)
            ub.append(u)

        ##Estimates of the forecast accuracy
        history = [hcost[0]]
        test = [hcost[i] for i in range(1, len(hcost))]
        pred = list()
        for k in range(len(test)):
            yhat = history[k] + (k + 1) * d
            obs = test[k]
            pred.append(yhat)
            history.append(obs)
            # print('predicted=%f, expected=%f' % (yhat, obs))

        ##Prediction Error
        rmse, mape = self.forecast_accuracy(test, pred)

        ##Create a dataframe with the predicitons
        df = pd.DataFrame()
        df = df.append(fseries)
        i = 0
        for index, row in df.iterrows():
            df[level_str].iloc[i] = forecast[i]
            i += 1
        df['Method'] = method_name
        df['Forecast_Identifier'] = '3'
        df['Historical_RMSE'] = rmse
        df['Mape'] = mape
        df['Standard_Error'] = rmserr
        df['CI_Lower'] = lb
        df['CI_Upper'] = ub

        return df, rmse

    ##4. Moving-average with drift
    def moving_average(self, hseries, fseries, level_str, window):
        """Moving average: The simple moving average method implemented here makes use of a
        "global" trend component to account for trends and seasonality in the time-series
        instead of de-trending first and then forecasting using the de-trended series.
        NOTE: The error bars around the forecast have been generated using prediction errors for
        (nobs - window) test data points. The estimates of standard error around the forecasted values
        therefore depend on the width of the moving average window specified."""
        ##Inputs:
        ##+
        ## series : The time-series for which the forecast is desired.
        ## cols: Column headings for the output file as per the standardised output format.
        ## window: The width of the window for the moving average. This can be an array of integers
        ## and has to be greater than 1. Right now this is set to the default value of 3.
        ## -
        method_name = 'Moving average with drift'
        hcost = hseries[level_str].values
        fdates = fseries['Time_Period'].values
        nf = len(fdates)
        n = len(hcost)
        ##NOTES: The total number of historical observations has to be greater than size of window = 3.
        ##If this is not the case, then skip this method and move on to the next
        ##Estimate the drift term
        gtrend = (hcost[n - 1] - hcost[0]) / (n - 1)
        history = [hcost[i] for i in range(window)]
        test_dat = [hcost[i] for i in range(window, len(hcost))]

        predictions = list()
        err_ma = list()
        # walk forward over time steps in test
        for t in range(len(test_dat)):
            length = len(history)
            yhat = np.mean([history[i] for i in range(length - window, length)]) + gtrend
            obs = test_dat[t]
            predictions.append(yhat)
            err_ma.append(yhat - obs)
            history.append(obs)
            # print('predicted=%f, expected=%f' % (yhat, obs))

        std_err_ma = np.std(err_ma)  ##RMS Error value
        ##This is the standard error around the 1-step step forecast.
        se_err_ma = std_err_ma
        rmse, mape = self.forecast_accuracy(test_dat, predictions)
        ##Calculate the error bars (95%) for each step k. For the simple moving average approach, the CI intervals
        ##DO NOT widen as the forecast horizon lengthens.
        lb = list()
        ub = list()
        t_c = stats.t.ppf(q=0.975,
                          df=n - 2)  ##The t-critical value from the standard t-distribution based on n-2 degrees of freedom.
        ##Generate the forecast beyond the observed cost points.
        forecast = list()
        for k in range(nf):
            tcost = np.mean(hcost[-1 * window:]) + (k + 1) * gtrend
            if tcost < 0.0:
                forecast.append(min(np.abs(tcost), hcost[n - 1]))
            else:
                forecast.append(tcost)
            l = forecast[k] - t_c * se_err_ma
            u = forecast[k] + t_c * se_err_ma
            ##This ensures that cost is a non-negative number!!
            if l < 0.0:
                l = 0.0
            lb.append(l)
            ub.append(u)

        ##Create a dataframe with the predicitons
        df = pd.DataFrame()
        df = df.append(fseries)
        i = 0
        for index, row in df.iterrows():
            df[level_str].iloc[i] = forecast[i]
            i += 1
        df['Method'] = method_name
        df['Forecast_Identifier'] = '4'
        df['Historical_RMSE'] = rmse
        df['Mape'] = mape
        df['Standard_Error'] = se_err_ma
        df['CI_Lower'] = lb
        df['CI_Upper'] = ub

        return df, rmse

    ##5. Simple exponential smoothing
    def simple_exponential_smoothing(self, hseries, fseries, level_str, alpha):
        """Simple Exponential smoothing (with drift): In simple exponential smoothing, each of the past observed values
        are used to predict future values. Intuitively, all the past values have some relevance,
        but each newer one is more relevant than older ones for predicting what is going to happen next.
        Therefore, past values are given gradually lesser weight (in an exponential manner) compared to more recent
        values. This implementation of the SES incorporates a constant trend component as well that is added at the
        end to accont for long-term trends and seasonality in the data."""

        ##Inputs:
        ##+
        ## series : The time-series for which the forecast is desired.
        ## cols: Column headings for the output file as per the standardised output format.
        ## alpha: The smoothing coefficient. A larger value (closer to 1) implies that recent values
        ## are more heavily weighted.
        ## -
        ##Function that calculates the exponentially smoothed estimates.
        def exponential_smoothing(series, alpha, flag):
            nt = len(series)
            result = [series[0]]  # first value is same as series
            if flag == 1:
                gtrend = (series[nt - 1] - series[0]) / (nt - 1)
                for n in range(1, len(series)):
                    result.append(alpha * series[n] + (1 - alpha) * result[n - 1] + gtrend)
            else:
                for n in range(1, len(series)):
                    result.append(alpha * series[n] + (1 - alpha) * result[n - 1])
            return result

        ##Extract basic data
        hcost = hseries[level_str].values
        fdates = fseries['Time_Period'].values
        nf = len(fdates)
        n = len(hcost)
        gtrend = (hcost[n - 1] - hcost[0]) / (n - 1)

        ##No drift
        method_name0 = 'Simple exponential smoothing'
        flag = 0
        result0 = exponential_smoothing(hcost, alpha, flag)
        # for r in range(len(result0)):
        #    print('predicted=%f, expected=%f' % (result0[r], hcost[r]))

        err_ses0 = np.array(result0) - np.array(hcost)
        se_err_ses0 = np.std(err_ses0)
        rmse0, mape0 = self.forecast_accuracy(hcost, result0)
        ##Calculate the error bars (95%) for each step k.
        lb0 = list()
        ub0 = list()
        rmserr0 = list()
        t_c = stats.t.ppf(q=0.975,
                          df=n - 1)  ##The t-critical value from the standard t-distribution based on n-2 degrees of freedom.
        ##Generate the forecast beyond the observed cost points.
        forecast0 = list()
        for k in range(nf):
            tcost = np.float(alpha * hcost[n - 1] + result0[-1] * (1 - alpha))
            if tcost < 0.0:
                forecast0.append(min(np.abs(tcost), hcost[n - 1]))
            else:
                forecast0.append(tcost)

            l = forecast0[k] - t_c * np.sqrt(1.0 + k * alpha ** 2) * se_err_ses0
            if l < 0.0:
                l = 0.0
            u = forecast0[k] + t_c * np.sqrt(1.0 + k * alpha ** 2) * se_err_ses0
            rmserr0.append(np.sqrt(1.0 + k * alpha ** 2) * se_err_ses0)
            lb0.append(l)
            ub0.append(u)

        ##With drift
        method_name1 = 'Simple exponential smoothing (with drift)'
        flag = 1
        result1 = exponential_smoothing(hcost, alpha, flag)
        # for r in range(len(result1)):
        #    print('predicted=%f, expected=%f' % (result1[r], hcost[r]))
        err_ses1 = np.array(result1) - np.array(hcost)
        se_err_ses1 = np.std(err_ses1)
        rmse1, mape1 = self.forecast_accuracy(hcost, result1)

        ##Calculate the error bars (95%) for each step k.
        lb1 = list()
        ub1 = list()
        rmserr1 = list()
        ##Generate the forecast beyond the observed cost points.
        forecast1 = list()
        for k in range(nf):
            tcost = np.float(alpha * hcost[n - 1] + result0[-1] * (1 - alpha) + (k + 1) * gtrend)
            if tcost < 0.0:
                forecast1.append(min(np.abs(tcost), hcost[n - 1]))
            else:
                forecast1.append(tcost)
            l = forecast1[k] - t_c * np.sqrt(1.0 + k * alpha ** 2) * se_err_ses1
            if l < 0.0:
                l = 0.0
            u = forecast1[k] + t_c * np.sqrt(1.0 + k * alpha ** 2) * se_err_ses1
            rmserr1.append(np.sqrt(1.0 + k * alpha ** 2) * se_err_ses1)
            lb1.append(l)
            ub1.append(u)

        ##Create a dataframe with the predicitons
        df0 = pd.DataFrame()
        df0 = df0.append(fseries)
        i = 0
        for index, row in df0.iterrows():
            df0[level_str].iloc[i] = forecast0[i]
            i += 1
        df0['Method'] = method_name0
        df0['Forecast_Identifier'] = '5'
        df0['Historical_RMSE'] = rmse0
        df0['Mape'] = mape0
        df0['Standard_Error'] = rmserr0
        df0['CI_Lower'] = lb0
        df0['CI_Upper'] = ub0

        ##Create a dataframe with the predicitons
        df1 = pd.DataFrame()
        df1 = df1.append(fseries)
        i = 0
        for index, row in df1.iterrows():
            df1[level_str].iloc[i] = forecast1[i]
            i += 1
        df1['Method'] = method_name1
        df1['Forecast_Identifier'] = '5'
        df1['Historical_RMSE'] = rmse1
        df1['Mape'] = mape1
        df1['Standard_Error'] = rmserr1
        df1['CI_Lower'] = lb1
        df1['CI_Upper'] = ub1

        ##Concatenate the two dataframes
        df = pd.concat([df0, df1], axis=0)
        return df1, rmse1

    ##7. Linear Regression
    def linear_regression(self, hseries, fseries, level_str):

        """Linear Regression: In this approach, we estimate the values of the
        slope and intercept of the linear equation Y = m*X + c by fitting the historical values
        which are then used to generate the forward forecast"""
        method_name = 'Linear Regression'
        hcost = hseries[level_str].values
        fdates = fseries['Time_Period'].values
        nf = len(fdates)
        n = len(hcost)

        ##Create temporary dataframe that contains Variable Time and the Variable Cost
        tdf = pd.DataFrame({'Time': np.arange(1, n + 1), 'Cost': np.array(hcost)})
        lm_s = smf.ols(formula='Cost ~ Time', data=tdf).fit()  ##Fit the historical cost data

        ##Now make predictions on the historical data
        pred = lm_s.predict(tdf)
        err = pred - hcost
        n = len(err)
        if n > 2:
            sderr_model = np.sqrt((n - 1) / (n - 2)) * np.std(err)
        else:
            sderr_model = np.std(err)
        ##Compute the forecast accuracy
        rmse, mape = self.forecast_accuracy(hcost, pred)

        ##Make predictions beyond the historical observations
        x_new = pd.DataFrame({'Time': np.arange(n + 1, (n + 1) + nf)})
        forecast = lm_s.predict(x_new)

        ##Getting the standard error and confidence intervals around the predicted estimates
        def transform_exog_to_model(fit, exog):
            transform = True
            self1 = fit

            # The following is lifted straight from statsmodels.base.model.Results.predict()
            if transform and hasattr(self1.model, 'formula') and exog is not None:

                exog = dmatrix(self1.model.data.orig_exog.design_info.builder,
                               exog)

                if exog is not None:
                    exog = np.asarray(exog)
                    if exog.ndim == 1 and (self1.model.exog.ndim == 1 or
                                                   self1.model.exog.shape[1] == 1):
                        exog = exog[:, None]
                    exog = np.atleast_2d(exog)  # needed in count model shape[1]

            # end lifted code
            return exog

        ##Use statsmodels.sandbox.regression.prestd.wls_prediction_std to get the
        ##confidence intervals and standard error around our predicted values for
        ##our new data (x_new). In order to do this, we first have to define a
        ##function called transform_exog_to_model that transforms the input x_new
        ##vector into the correct format that is required by wls_prediction_std

        transformed_exog = transform_exog_to_model(lm_s, x_new)
        prstd, iv_l, iv_u = wls_prediction_std(lm_s, transformed_exog)

        t_c = stats.t.ppf(q=0.975, df=n - 2)
        for i in range(nf):
            if forecast[i] < 0.0:
                forecast[i] = min(hcost[-1], np.abs(forecast[i]))
                if forecast[i] == hcost[-1]:
                    iv_l[i] = forecast[i] - sderr_model * t_c
                    if iv_l[i] < 0.0:
                        iv_l[i] = 1e-7
                    iv_u[i] = forecast[i] + sderr_model * t_c
                    prstd[i] = sderr_model
                else:
                    iv_l[i] = forecast[i] - prstd[i] * t_c
                    if iv_l[i] < 0.0:
                        iv_l[i] = 1e-7
                    iv_u[i] = forecast[i] + prstd[i] * t_c
            else:
                if iv_l[i] < 0.0:
                    iv_l[i] = 1e-7

        ##Create a dataframe with the predicitons
        df = pd.DataFrame()
        df = df.append(fseries)
        i = 0
        for index, row in df.iterrows():
            df[level_str].iloc[i] = forecast[i]
            i += 1
        df['Method'] = method_name
        df['Forecast_Identifier'] = '6'
        df['Historical_RMSE'] = rmse
        df['Mape'] = mape
        df['Standard_Error'] = prstd
        df['CI_Lower'] = iv_l
        df['CI_Upper'] = iv_u

        return df, rmse

    ##8. ARIMA (1,1,0): That corresponds to a differenced first order auto-regressive model
    def arima(self, hseries, fseries, level_str):
        """ARIMA: We implement the (1,1,0) arima model that corresponds to a differenced first order
        auto-regressive model AR(1) where in the AR(1) component is used to eliminate or reduce any positive
        autocorrelations in the differenced series.
        """

        method = 'ARIMA'
        nf = len(fseries)
        n = len(hseries)
        size = int(n * 0.66)
        train, test = hseries[level_str][0:size], hseries[level_str][size:len(hseries)]
        history = [x for x in train]
        test = [x for x in test]

        predictions = list()
        for t in range(len(test)):
            try:
                model = ARIMA(history, order=(1, 1, 0))
                order = (1, 1, 0)
                try:
                    model_fit = model.fit(disp=0)
                    method_name = 'ARIMA ' + str(order)
                    output = model_fit.forecast(steps=1)
                    yhat = np.abs(output[0][0])
                    if np.isnan(yhat):
                        yhat = test[t]
                    predictions.append(yhat)
                    obs = test[t]
                    history.append(obs)
                except(ValueError, linalgerr):
                    # print("Reverting to a naive forecast")
                    order = (0, 1, 0)
                    method_name = 'ARIMA ' + str(order)
                    yhat = history[-1]
                    predictions.append(yhat)
                    obs = test[t]
            except(ValueError):
                # print("Reverting to a naive forecast")
                order = (0, 1, 0)
                method_name = 'ARIMA ' + str(order)
                yhat = history[-1]
                predictions.append(yhat)
                obs = test[t]

                # print('predicted=%f, expected=%f' % (yhat, obs))

        ##Compute RMSE and MAPE
        rmse, mape = self.forecast_accuracy(test, predictions)

        t_c = stats.t.ppf(q=0.975, df=n - 1)
        ##Generate out-of-sample forecasts
        if order == (1, 1, 0):
            forecast, stderr, conf = model_fit.forecast(steps=4)
            ##Check for negative forecasts: If there are any negative forecasts, then revert back to the naive
            ##forecast.
            nmask = np.array(forecast) < 0.0
            if len(nmask) > 0:
                forecast = list()
                stderr = list()
                conf = np.zeros((nf, 2))
                for k in range(nf):
                    forecast.append(test[-1])
                    stderr.append(rmse)
                    conf[k][0] = forecast[k] - t_c * stderr[k] * np.sqrt(k + 1)
                    conf[k][1] = forecast[k] + t_c * stderr[k] * np.sqrt(k + 1)
        else:
            # print("Reverting to a naive forecast")
            forecast = list()
            stderr = list()
            conf = np.zeros((nf, 2))
            for k in range(nf):
                forecast.append(test[-1])
                stderr.append(rmse)
                conf[k][0] = forecast[k] - t_c * stderr[k] * np.sqrt(k + 1)
                conf[k][1] = forecast[k] + t_c * stderr[k] * np.sqrt(k + 1)

        ##Sanity check for negative error bounds.
        lb = list()
        ub = list()
        for i in range(nf):
            if conf[i][0] < 0.0:
                conf[i][0] = 0.0
            lb.append(conf[i][0])
            ub.append(conf[i][1])

        ##Create a dataframe with the predicitons
        df = pd.DataFrame()
        df = df.append(fseries)
        i = 0
        for index, row in df.iterrows():
            df[level_str].iloc[i] = forecast[i]
            i += 1
        df['Method'] = method_name
        df['Forecast_Identifier'] = '7'
        df['Historical_RMSE'] = rmse
        df['Mape'] = mape
        df['Standard_Error'] = stderr
        df['CI_Lower'] = lb
        df['CI_Upper'] = ub

        return df, rmse, method_name

    def forecast_cost(self, hseries, fseries, level, level_str, colsj):
        """"""
        nf = len(fseries)
        if len(hseries) == 1:
            """Only naive prediction is possible in this case."""
            df, rerr = self.random_walk(hseries, fseries, level_str)
            bvec = np.array(['True' for x in range(nf)])
            jvec = 'Only one data point has been provided for this MPN_ID. Therefore, only the naive method can be used to generate the forecasts. In this case, error metrics such as RMSE and MAPE are undefined.'
            df['Selected'] = bvec
            dfj = df[colsj].iloc[0]
            dfj['Forecasted_Level'] = level
            dfj['Forecasted_Quantity'] = level_str
            dfj['Justification'] = jvec
        elif len(hseries) == 2:
            """Except for MA all other approaches possible!!"""
            df0, r0 = self.simple_average(hseries, fseries, level_str)
            df1, r1 = self.random_walk(hseries, fseries, level_str)
            df2, r2 = self.random_walk_drift(hseries, fseries, level_str)
            alpha = 0.65  ##This has been hard coded
            df3, r3 = self.simple_exponential_smoothing(hseries, fseries, level_str, alpha)
            df4, r4 = self.linear_regression(hseries, fseries, level_str)
            df5, r5, mn = self.arima(hseries, fseries, level_str)
            ##Find the min value among all the RMSEs
            rmse = {0: r0, nf: r1, 2 * nf: r2, 3 * nf: r3, 4 * nf: r4, 5 * nf: r5}
            bvec = np.array(['False' for x in range(len(rmse) * nf)])
            # keys = list(rmse.keys())
            ind = min(rmse, key=rmse.get)
            bvec[ind:ind + nf] = 'True'
            ##Concatenate the dataframes into a single data frame
            df = pd.concat([df0, df1, df2, df3, df4, df5])
            jvec = 'The selected method is based on one that gives the minimum RMSE. Additionally with only two data points, the moving average is also unavailable to make predictions.'
            df['Selected'] = bvec
            dfj = df[colsj].iloc[ind]
            dfj['Forecasted_Level'] = level
            dfj['Forecasted_Quantity'] = level_str
            dfj['Justification'] = jvec
        elif len(hseries) == 3:
            """All methods can be used to make predictions. For MA, window size has to be equal to 2"""
            df0, r0 = self.simple_average(hseries, fseries, level_str)
            df1, r1 = self.random_walk(hseries, fseries, level_str)
            df2, r2 = self.random_walk_drift(hseries, fseries, level_str)
            ##Width of the moving average window. Default is set to 3
            window = 2
            df3, r3 = self.moving_average(hseries, fseries, level_str, window)
            alpha = 0.65  ##This has been hard coded
            df4, r4 = self.simple_exponential_smoothing(hseries, fseries, level_str, alpha)
            df5, r5 = self.linear_regression(hseries, fseries, level_str)
            df6, r6, mn = self.arima(hseries, fseries, level_str)
            ##Find the min value among all the RMSEs
            rmse = {0: r0, nf: r1, 2 * nf: r2, 3 * nf: r3, 4 * nf: r4, 5 * nf: r5, 6 * nf: r6}
            bvec = np.array(['False' for x in range(len(rmse) * nf)])
            # keys = list(rmse.keys())
            ind = min(rmse, key=rmse.get)
            bvec[ind:ind + nf] = 'True'
            ##Concatenate the dataframes into a single data frame
            df = pd.concat([df0, df1, df2, df3, df4, df5, df6])
            # display(df0)
            jvec = 'The selected method is based on one that gives the minimum RMSE. Additionally with three data points, only the moving average with window width set equal to 2 is available to make predictions.'
            df['Selected'] = bvec
            dfj = df[colsj].iloc[ind]
            dfj['Forecasted_Level'] = level
            dfj['Forecasted_Quantity'] = level_str
            dfj['Justification'] = jvec
        else:  ##For npoints > 3
            """All methods can be used to make predictions."""
            df0, r0 = self.simple_average(hseries, fseries, level_str)
            df1, r1 = self.random_walk(hseries, fseries, level_str)
            df2, r2 = self.random_walk_drift(hseries, fseries, level_str)
            ##Width of the moving average window. Default is set to 3
            window = 3
            df3, r3 = self.moving_average(hseries, fseries, level_str, window)
            alpha = 0.65  ##This has been hard coded
            df4, r4 = self.simple_exponential_smoothing(hseries, fseries, level_str, alpha)
            df5, r5 = self.linear_regression(hseries, fseries, level_str)
            df6, r6, mn = self.arima(hseries, fseries, level_str)
            ##Find the min value among all the RMSEs
            rmse = {0: r0, nf: r1, 2 * nf: r2, 3 * nf: r3, 4 * nf: r4, 5 * nf: r5, 6 * nf: r6}
            bvec = np.array(['False' for x in range(len(rmse) * nf)])
            # keys = list(rmse.keys())
            ind = min(rmse, key=rmse.get)
            bvec[ind:ind + nf] = 'True'
            ##Concatenate the dataframes into a single data frame
            df = pd.concat([df0, df1, df2, df3, df4, df5, df6])
            # display(df0)
            jvec = 'The selected method is based on one that gives the minimum RMSE. Consequently, the selected methods also has the lowest MAPE.'
            df['Selected'] = bvec
            dfj = df[colsj].iloc[ind]
            dfj['Forecasted_Level'] = level
            dfj['Forecasted_Quantity'] = level_str
            dfj['Justification'] = jvec

        ##Convert dfj to a dataframe
        dfj = dfj.to_frame().transpose()
        return df, dfj


if __name__ == '__main__':

    ##Use sysv to get filenames from command line
    nargs = len(sys.argv[1:])
    fpath = str(sys.argv[1])
    cust = str(sys.argv[2])
    items = []
    if nargs == 5:
        levels = sys.argv[3].strip('[]').split(',')
        if len(levels) > 1:
            sys.exit(
                'Error: Only one base level needs to be specified. %d found. PLease enter only one base level.' % len(
                    levels))
        else:
            items = sys.argv[4].strip('[]').split(',')
            periods = int(sys.argv[5])
            print("Forecasts will be generated at the %s level for %d items." % (levels[0], len(items)))
    else:
        levels = sys.argv[3].strip('[]').split(',')
        print("Forecasts will be generated at the %s levels" % levels)
        periods = int(sys.argv[4])

    fc = Forecast()
    fc.executetextinput(fpath,cust,levels,periods,items,'text')
    #fc.executedbinput(fpath, cust, levels, periods, items,'db')