from statsmodels.sandbox.regression.predstd import wls_prediction_std
from patsy import dmatrix
import statsmodels.api as sm
import scipy.stats as stats
import numpy as np
import pandas as pd
from sklearn.metrics import mean_squared_error
from IPython.display import display
import sys
class Regressor(object):
    """This class contains a method that can be used to any series against multiple predictor series."""
    def __init__(self):

        #self.l1 = level1_str
        #self.l2 = level2_str

        self.cols = ['MPN_Unit_Cost', 'Method', 'Forecast Identifier',
                'Historical Root-Mean Square Error (RMSE)', 'Mean Absolute Percentage Error (MAPE)',
                'Standard Error (approx RMS)', '95% CI (Lower)', '95% CI (Upper)', 'Selected']


    def regression(self, idf, fdf, fseries, l1, lvls, find):
        """
        This method regresses the dependent series against multiple predictors and generates a dataframe containing
        the predictions and other metrics. Here lvls is a list of strings that correspond to the names of the predictors.
        """

        X = idf[lvls]
        X = sm.add_constant(X, has_constant='add')
        y = idf[l1]

        ##Fit the model
        est = sm.OLS(y, X).fit()

        ##Now make predictions within the sample
        pred_X = est.predict(X)
        err = pred_X - y
        n = len(err)
        if n > 2:
            sderr_model = np.sqrt((n - 1) / (n - 2)) * np.std(err)
        else:
            sderr_model = np.std(err)
        

        ##Get the in-sample error metrics
        rmse = mean_squared_error(y, pred_X)
        mape = (np.mean(np.abs(np.asarray(err) / np.asarray(y)))) * 100

        ##Predict using the future values of the independent variable

        X_new = fdf[lvls]
        X_new = sm.add_constant(X_new, has_constant='add')
        pred  = np.asarray(est.predict(X_new))

        ##Obtain the prediction standard error and confidence intervals
        ##Getting the standard error and confidence intervals around the predicted estimates
        def transform_exog_to_model(fit, exog):
            transform = True
            self1 = fit

            # The following is lifted straight from statsmodels.base.model.Results.predict()
            if transform and hasattr(self1.model, 'formula') and exog is not None:

                exog = dmatrix(self1.model.data.orig_exog.design_info.builder,
                               exog)

                if exog is not None:
                    exog = np.asarray(exog)
                    if exog.ndim == 1 and (self1.model.exog.ndim == 1 or
                                                   self1.model.exog.shape[1] == 1):
                        exog = exog[:, None]
                    exog = np.atleast_2d(exog)  # needed in count model shape[1]

            # end lifted code
            return exog

        ##Use statsmodels.sandbox.regression.prestd.wls_prediction_std to get the
        ##confidence intervals and standard error around our predicted values for
        ##our new data (x_new). In order to do this, we first have to define a
        ##function called transform_exog_to_model that transforms the input x_new
        ##vector into the correct format that is required by wls_prediction_std

        transformed_exog = transform_exog_to_model(est, X_new)
        try:
            prstd, iv_l, iv_u = wls_prediction_std(est, transformed_exog, weights=1)
        except(ValueError):
            prstd = np.zeros(len(pred))
            iv_l  = np.zeros(len(pred))
            iv_u  = np.zeros(len(pred))

        t_c = stats.t.ppf(q=0.975, df=n - 2)
        ##Write everything out to a dataframe. We also check to see if the forecasted values are negative and if so
        ##reflect the values about the x-axis to make the predictions positive.
        if 'SubCommodity' in lvls or 'Commodity' in lvls:
            title = 'Regression on ' + lvls
        else:
            title = 'Regression on material and exogenous factors'
        df   = pd.DataFrame()
        df   = df.append(fseries)
        df = df.reset_index()
        for i in range(len(pred)):
            ##First check to see if prediction is positive or negative
            if pred[i] < 0.0:
                pred[i] = min(y.values[-1], np.abs(pred[i]))
                if pred[i] == y.values[-1]:
                    iv_l[i] = pred[i] - sderr_model * t_c
                    if iv_l[i] < 0.0:
                        iv_l[i] = 1e-7
                    iv_u[i] = pred[i] + sderr_model * t_c
                    prstd[i] = sderr_model
                else:
                    iv_l[i] = pred[i] - prstd[i] * t_c
                    if iv_l[i] < 0.0:
                        iv_l[i] = 1e-7
                    iv_u[i] = pred[i] + prstd[i] * t_c
            else:
                if iv_l[i] < 0.0:
                    iv_l[i] = 1e-7
        ##Now populate df
        df.loc[:,l1] = pd.Series(pred)
        df['Method'] = title
        df.loc[:, 'Forecast_Identifier'] = str(find)
        df.loc[:,'Historical_RMSE'] = rmse
        df.loc[:,'Mape'] = mape
        df.loc[:,'Standard_Error'] = prstd
        df.loc[:,'CI_Lower'] = iv_l
        df.loc[:,'CI_Upper'] = iv_u
        df = df.drop('index', axis=1)
        return df
