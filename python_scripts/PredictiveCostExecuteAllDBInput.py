from Forecast_Regress_External import *
from Forecast_Regress_Internal import *
from Forecasting import *
from Driver_Forecast import *
import configparser

class PredictiveCostExecuteAllDBInput(object):

    def predictivecostdbinput(self):
        starttime = time.time()
        config = configparser.RawConfigParser()
        config.read('config.properties')

        print("Starting predictive cost")

        print("Executing Forecasting.py")

        fpath = config.get('Forecasting', 'fpath')
        cust = config.get('Forecasting', 'customer')
        levels = eval(config.get('Forecasting', 'levels'), {}, {})
        periods = int(config.get('Forecasting', 'periods'))
        items = eval(config.get('Forecasting', 'items'), {}, {})
        outputtype = config.get('Forecasting', 'outputtype')

        forecast = Forecast()
        forecast.executedbinput(fpath, cust, levels, periods, items, outputtype)

        print("Executed Forecasting.py")

        print("Executing Forecast_Regress_Internal.py")

        fpath = config.get('Forecasting_Regress_Internal', 'fpath')
        cust = config.get('Forecasting_Regress_Internal', 'customer')
        level1 = config.get('Forecasting_Regress_Internal', 'level1')
        levels = eval(config.get('Forecasting_Regress_Internal', 'levels'), {}, {})
        items = eval(config.get('Forecasting_Regress_Internal', 'items'), {}, {})
        outputtype = config.get('Forecasting_Regress_Internal', 'outputtype')

        fr = ForecastRegressInternal()
        fr.executedbinput(fpath, cust, level1, levels, items, outputtype)

        print("Executed Forecast_Regress_Internal.py")

        print("Executing Driver_Forecast.py")

        fpath = config.get('Driver_Forecast', 'fpath')
        cust = config.get('Driver_Forecast', 'customer')
        dur = int(config.get('Driver_Forecast', 'dur'))
        interval = int(config.get('Driver_Forecast', 'interval'))
        levels = eval(config.get('Driver_Forecast', 'levels'), {}, {})
        outputtype = config.get('Driver_Forecast', 'outputtype')

        dfore = DriverForecast()
        dfore.executetextinput(fpath, cust, dur, interval, levels, outputtype)


        print("Executed Driver_Forecast.py")

        print("Executing Forecast_regress_External.py")

        fpath = config.get('Forecast_Regress_External', 'fpath')
        cust = config.get('Forecast_Regress_External', 'customer')
        flag = int(config.get('Forecast_Regress_External', 'flag'))
        level1 = config.get('Forecast_Regress_External', 'level1')
        levels = eval(config.get('Forecast_Regress_External', 'levels'), {}, {})
        items = eval(config.get('Forecast_Regress_External', 'items'), {}, {})
        outputtype = config.get('Forecasting_Regress_Internal', 'outputtype')

        frext = ForecastRegressExternal()
        frext.executedbinput(fpath, cust, flag, level1, levels, items, outputtype)
        print('Total Time taken  is %s' % (time.time() - starttime))
        print("Executed Forecast_regress_external.py")


if __name__ == '__main__':
    print("Starting predictive cost")
    pd = PredictiveCostExecuteAllDBInput()
    pd.predictivecostdbinput()
