##Modules to import
import pandas as pd
import numpy as np
import statsmodels.formula.api as smf
import scipy.stats as stats
from sklearn.metrics import mean_squared_error
from statsmodels.sandbox.regression.predstd import wls_prediction_std
from patsy import dmatrix
from statsmodels.tsa.arima_model import ARIMA
from numpy.linalg.linalg import LinAlgError as linalgerr
import sys
from IPython.display import display
from scipy.optimize import bisect, brentq
import time
##The predictor class
class Predict(object):

    ##Forecast error metrics
    def forecast_accuracy(self, test, pred):

        '''This function calculates several error metrics to evaluate forecast accuracy'''
        #mse = mean_squared_error(test, pred)
        # print("The mean square error using the historical mean to predict observed data is %3f" % mse)
        ##Standard error = square root of the mean squared error or root-mean square error
        rmse = mean_squared_error(test, pred) ** 0.5
        # print("The standard or root mean square error using the historical mean to predict observed data is %3f" % rmse)
        ##MAPE (Mean Absolute Percentage Error)
        if all(i > 0.0 for i in test):
            err = np.array(pred) - np.array(test)
            mape = (np.mean(np.abs(np.asarray(err) / np.asarray(test)))) * 100
            # print("The mean absolute percentage error using the historical mean to predict observed data is %3f%%" % mape)
        else:
            mape = 'NaN'
        return rmse, mape

    ##1. The simple mean model
    def simple_average(self, hseries, fseries, level_str):
        """Simple historical average: Calculate the simple average of all the historical observations and use that
           as an estimate of the predicted cost for all future periods."""

        method_name = 'Mean Model'
        hcost = hseries[level_str].values
        n = len(hcost)
        cost = hcost

        ##Compute the historical average
        avg = cost.mean()
        pred = np.array([avg for _ in range(len(cost))])

        rmse, mape = self.forecast_accuracy(cost, pred)

        ##Calculate standard error and confidence intervals
        err = pred - hcost
        sd_err = np.std(err)  ##Model standard error
        fserr = np.sqrt((sd_err) ** 2 + (sd_err) ** 2 / len(hcost))  ##Forecast standard error (1-step)
        ##CIs: The CI's for the mean model do not grow in time as the forecast horizon
        t_critical = stats.t.ppf(q=0.975,
                                 df=n - 1)  ##This is used instead of the z-value since the population std dev is not known
        lb = avg - t_critical * fserr
        if lb < 0.0:
            lb = 0.0
        ub = avg + t_critical * fserr

        ##Create a dataframe with the predicitons
        df0 = pd.DataFrame()
        df0 = df0.append(fseries)
        i = 0
        for index, row in df0.iterrows():
            df0[level_str].iloc[i] = avg
            i += 1

        df0['Method'] = method_name
        df0['Forecast_Identifier'] = '1'
        df0[level_str+'_Historical_RMSE'] = rmse
        df0[level_str+'_Mape'] = mape
        df0[level_str+'_Standard_Error'] = fserr
        df0[level_str+'_CI_Lower'] = lb
        df0[level_str+'_CI_Upper'] = ub

        return df0, rmse

        ##2. Random walk
    def random_walk(self, hseries, fseries, level_str):
        """Random-walk: This method is based on the assumption that the past observations provide NO
        information on the future direction (trend) of the series. The RW with drift is
        just like the random-walk but with an additive drift component that corresponds
        to the average period-to-period change observed in the past. This drift component pertains to the
        mean step-size."""

        method_name = 'Random walk'
        hcost  = hseries[level_str].values
        nf = len(fseries.index.values)
        n = len(hcost)

        ##Make out-of sample forecasts
        sd_err = np.std(np.diff(hcost))

        ##Forecast from the last observed point out.
        t_c = stats.t.ppf(q=0.975, df=n - 2)
        forecast = list()
        lb = list()
        ub = list()
        rmserr = list()
        for k in range(nf):
            forecast.append(hcost[n - 1])
            l = forecast[k] - t_c * sd_err * np.sqrt(k + 1)
            u = forecast[k] + t_c * sd_err * np.sqrt(k + 1)
            if l < 0:
                l = 0.0
            rmserr.append(sd_err * np.sqrt(k + 1))
            lb.append(l)
            ub.append(u)

        if n == 1:
            ##If there is only one-point available, then rmse/mape is not defined.
            rmse = 0.0
            mape = 0.0

        else:
            ##Estimates of the forecast accuracy
            history = [hcost[0]]
            test = [hcost[i] for i in range(1, len(hcost))]
            pred = list()
            for k in range(len(test)):
                yhat = history[k]
                obs = test[k]
                pred.append(yhat)
                history.append(obs)
                # print('predicted=%f, expected=%f' % (yhat, obs))

            ##Prediction Error
            rmse, mape = self.forecast_accuracy(test, pred)

        ##Create a dataframe with the predicitons
        df = pd.DataFrame()
        df = df.append(fseries)
        i = 0
        for index, row in df.iterrows():
            df[level_str].iloc[i] = forecast[i]
            i += 1
        df['Method'] = method_name
        df['Forecast_Identifier'] = '2'
        df[level_str+'_Historical_RMSE'] = rmse
        df[level_str+'_Mape'] = mape
        df[level_str+'_Standard_Error'] = rmserr
        df[level_str+'_CI_Lower'] = lb
        df[level_str+'_CI_Upper'] = ub

        return df, rmse

    ##3. Random-walk with drift
    def random_walk_drift(self, hseries, fseries, level_str):
        """Random-walk with drift: The RW with drift is just like the random-walk but
        with an additive drift component that corresponds to the average period-to-period
        change observed in the past. This drift component pertains to the mean step-size."""

        method_name = 'Random walk with drift'
        hcost = np.array(hseries[level_str].values)
        nf = len(fseries.index.values)
        n = len(hcost)
        ##Estimate the drift term
        d = (hcost[n - 1] - hcost[0]) / (n - 1)

        ##Estimate the standard error of the k-step ahead forecast
        sd_err = np.std(np.diff(hcost))

        ##Forecast from the last observed point out.
        t_c = stats.t.ppf(q=0.975, df=n - 2)
        forecast = list()
        lb = list()
        ub = list()
        rmserr = list()
        for k in range(nf):
            tcost = hcost[-1] + (k + 1) * d
            # print(k, hcost[-1], tcost, min(hcost[-1], np.abs(tcost)))
            if tcost < 0.0:
                ##Reflect the value
                forecast.append(min(hcost[-1], np.abs(tcost)))
            else:
                forecast.append(tcost)

            l = forecast[k] - t_c * sd_err * np.sqrt(k + 1)
            u = forecast[k] + t_c * sd_err * np.sqrt(k + 1)
            if l < 0:
                l = 0.0
            rmserr.append(sd_err * np.sqrt(k + 1))
            lb.append(l)
            ub.append(u)

        ##Estimates of the forecast accuracy
        history = [hcost[0]]
        test = [hcost[i] for i in range(1, len(hcost))]
        pred = list()
        for k in range(len(test)):
            yhat = history[k] + (k + 1) * d
            obs = test[k]
            pred.append(yhat)
            history.append(obs)
            # print('predicted=%f, expected=%f' % (yhat, obs))

        ##Prediction Error
        rmse, mape = self.forecast_accuracy(test, pred)

        ##Create a dataframe with the predicitons
        df = pd.DataFrame()
        df = df.append(fseries)
        i = 0
        for index, row in df.iterrows():
            df[level_str].iloc[i] = forecast[i]
            i += 1
        df['Method'] = method_name
        df['Forecast_Identifier'] = '3'
        df[level_str+'_Historical_RMSE'] = rmse
        df[level_str+'_Mape'] = mape
        df[level_str+'_Standard_Error'] = rmserr
        df[level_str+'_CI_Lower'] = lb
        df[level_str+'_CI_Upper'] = ub

        return df, rmse

    ##4. Moving-average with drift
    def moving_average(self, hseries, fseries, level_str, window):
        """Moving average: The simple moving average method implemented here makes use of a
        "global" trend component to account for trends and seasonality in the time-series
        instead of de-trending first and then forecasting using the de-trended series.
        NOTE: The error bars around the forecast have been generated using prediction errors for
        (nobs - window) test data points. The estimates of standard error around the forecasted values
        therefore depend on the width of the moving average window specified."""
        ##Inputs:
        ##+
        ## series : The time-series for which the forecast is desired.
        ## cols: Column headings for the output file as per the standardised output format.
        ## window: The width of the window for the moving average. This can be an array of integers
        ## and has to be greater than 1. Right now this is set to the default value of 3.
        ## -
        method_name = 'Moving average with drift'
        hcost = hseries[level_str].values
        nf = len(fseries.index.values)
        n = len(hcost)
        ##NOTES: The total number of historical observations has to be greater than size of window = 3.
        ##If this is not the case, then skip this method and move on to the next
        ##Estimate the drift term
        gtrend = (hcost[n - 1] - hcost[0]) / (n - 1)
        history = [hcost[i] for i in range(window)]
        test_dat = [hcost[i] for i in range(window, len(hcost))]

        predictions = list()
        err_ma = list()
        # walk forward over time steps in test
        for t in range(len(test_dat)):
            length = len(history)
            yhat = np.mean([history[i] for i in range(length - window, length)]) + gtrend
            obs = test_dat[t]
            predictions.append(yhat)
            err_ma.append(yhat - obs)
            history.append(obs)
            # print('predicted=%f, expected=%f' % (yhat, obs))

        std_err_ma = np.std(err_ma)  ##RMS Error value
        ##This is the standard error around the 1-step step forecast.
        se_err_ma = std_err_ma
        rmse, mape = self.forecast_accuracy(test_dat, predictions)
        ##Calculate the error bars (95%) for each step k. For the simple moving average approach, the CI intervals
        ##DO NOT widen as the forecast horizon lengthens.
        lb = list()
        ub = list()
        t_c = stats.t.ppf(q=0.975,
                          df=n - 2)  ##The t-critical value from the standard t-distribution based on n-2 degrees of freedom.
        ##Generate the forecast beyond the observed cost points.
        forecast = list()
        for k in range(nf):
            tcost = np.mean(hcost[-1 * window:]) + (k + 1) * gtrend
            if tcost < 0.0:
                forecast.append(min(np.abs(tcost), hcost[n - 1]))
            else:
                forecast.append(tcost)
            l = forecast[k] - t_c * se_err_ma
            u = forecast[k] + t_c * se_err_ma
            ##This ensures that cost is a non-negative number!!
            if l < 0.0:
                l = 0.0
            lb.append(l)
            ub.append(u)

        ##Create a dataframe with the predicitons
        df = pd.DataFrame()
        df = df.append(fseries)
        i = 0
        for index, row in df.iterrows():
            df[level_str].iloc[i] = forecast[i]
            i += 1
        df['Method'] = method_name
        df['Forecast_Identifier'] = '4'
        df[level_str+'_Historical_RMSE'] = rmse
        df[level_str+'_Mape'] = mape
        df[level_str+'_Standard_Error'] = se_err_ma
        df[level_str+'_CI_Lower'] = lb
        df[level_str+'_CI_Upper'] = ub

        # ##Create an empty dataframe
        # index = range(len(fdates))
        # df2 = pd.DataFrame(columns=self.cols_output)
        # ##Generate forecast for fdates:
        # i = 0
        # for index, row in fseries.iterrows():
        #     temp_dict = {}
        #     temp = [str(row[self.cols_output[x]]) for x in range(len(self.cols_output[0:9]))]
        #     temp.append(forecast[i])
        #     temp.append(method_name)
        #     temp.append('3')
        #     temp.append(rmse)
        #     temp.append(mape)
        #     temp.append(se_err_ma)
        #     temp.append(lb[i])
        #     temp.append(ub[i])
        #     for c in range(len(self.cols_output)):
        #         temp_dict[self.cols_output[c]] = temp[c]
        #     df2.loc[index] = pd.Series(temp_dict)
        #     i = i + 1

        return df, rmse

    ##5. Simple exponential smoothing
    def simple_exponential_smoothing(self, hseries, fseries, level_str, alpha):
        """Simple Exponential smoothing (with drift): In simple exponential smoothing, each of the past observed values
        are used to predict future values. Intuitively, all the past values have some relevance,
        but each newer one is more relevant than older ones for predicting what is going to happen next.
        Therefore, past values are given gradually lesser weight (in an exponential manner) compared to more recent
        values. This implementation of the SES incorporates a constant trend component as well that is added at the
        end to accont for long-term trends and seasonality in the data."""

        ##Inputs:
        ##+
        ## series : The time-series for which the forecast is desired.
        ## cols: Column headings for the output file as per the standardised output format.
        ## alpha: The smoothing coefficient. A larger value (closer to 1) implies that recent values
        ## are more heavily weighted.
        ## -
        ##Function that calculates the exponentially smoothed estimates.
        def exponential_smoothing(series, alpha, flag):
            nt = len(series)
            result = [series[0]]  # first value is same as series
            if flag == 1:
                gtrend = (series[nt - 1] - series[0]) / (nt - 1)
                for n in range(1, len(series)):
                    result.append(alpha * series[n] + (1 - alpha) * result[n - 1] + gtrend)
            else:
                for n in range(1, len(series)):
                    result.append(alpha * series[n] + (1 - alpha) * result[n - 1])
            return result

        ##Extract basic data
        hcost = hseries[level_str].values
        nf = len(fseries.index.values)
        n = len(hcost)
        gtrend = (hcost[n - 1] - hcost[0]) / (n - 1)

        ##No drift
        method_name0 = 'Simple exponential smoothing'
        flag = 0
        result0 = exponential_smoothing(hcost, alpha, flag)
        # for r in range(len(result0)):
        #    print('predicted=%f, expected=%f' % (result0[r], hcost[r]))

        err_ses0 = np.array(result0) - np.array(hcost)
        se_err_ses0 = np.std(err_ses0)
        rmse0, mape0 = self.forecast_accuracy(hcost, result0)
        ##Calculate the error bars (95%) for each step k.
        lb0 = list()
        ub0 = list()
        rmserr0 = list()
        t_c = stats.t.ppf(q=0.975,
                          df=n - 1)  ##The t-critical value from the standard t-distribution based on n-2 degrees of freedom.
        ##Generate the forecast beyond the observed cost points.
        forecast0 = list()
        for k in range(nf):
            tcost = np.float(alpha * hcost[n - 1] + result0[-1] * (1 - alpha))
            if tcost < 0.0:
                forecast0.append(min(np.abs(tcost), hcost[n - 1]))
            else:
                forecast0.append(tcost)

            l = forecast0[k] - t_c * np.sqrt(1.0 + k * alpha ** 2) * se_err_ses0
            if l < 0.0:
                l = 0.0
            u = forecast0[k] + t_c * np.sqrt(1.0 + k * alpha ** 2) * se_err_ses0
            rmserr0.append(np.sqrt(1.0 + k * alpha ** 2) * se_err_ses0)
            lb0.append(l)
            ub0.append(u)

        ##With drift
        method_name1 = 'Simple exponential smoothing (with drift)'
        flag = 1
        result1 = exponential_smoothing(hcost, alpha, flag)
        # for r in range(len(result1)):
        #    print('predicted=%f, expected=%f' % (result1[r], hcost[r]))
        err_ses1 = np.array(result1) - np.array(hcost)
        se_err_ses1 = np.std(err_ses1)
        rmse1, mape1 = self.forecast_accuracy(hcost, result1)

        ##Calculate the error bars (95%) for each step k.
        lb1 = list()
        ub1 = list()
        rmserr1 = list()
        ##Generate the forecast beyond the observed cost points.
        forecast1 = list()
        for k in range(nf):
            tcost = np.float(alpha * hcost[n - 1] + result0[-1] * (1 - alpha) + (k + 1) * gtrend)
            if tcost < 0.0:
                forecast1.append(min(np.abs(tcost), hcost[n - 1]))
            else:
                forecast1.append(tcost)
            l = forecast1[k] - t_c * np.sqrt(1.0 + k * alpha ** 2) * se_err_ses1
            if l < 0.0:
                l = 0.0
            u = forecast1[k] + t_c * np.sqrt(1.0 + k * alpha ** 2) * se_err_ses1
            rmserr1.append(np.sqrt(1.0 + k * alpha ** 2) * se_err_ses1)
            lb1.append(l)
            ub1.append(u)

        ##Create a dataframe with the predicitons
        df0 = pd.DataFrame()
        df0 = df0.append(fseries)
        i = 0
        for index, row in df0.iterrows():
            df0[level_str].iloc[i] = forecast0[i]
            i += 1
        df0['Method'] = method_name0
        df0['Forecast_Identifier'] = '5'
        df0[level_str+'_Historical_RMSE'] = rmse0
        df0[level_str+'_Mape'] = mape0
        df0[level_str+'_Standard_Error'] = rmserr0
        df0[level_str+'_CI_Lower'] = lb0
        df0[level_str+'_CI_Upper'] = ub0

        ##Create a dataframe with the predicitons
        df1 = pd.DataFrame()
        df1 = df1.append(fseries)
        i = 0
        for index, row in df1.iterrows():
            df1[level_str].iloc[i] = forecast1[i]
            i += 1
        df1['Method'] = method_name1
        df1['Forecast_Identifier'] = '5'
        df1[level_str+'_Historical_RMSE'] = rmse1
        df1[level_str+'_Mape'] = mape1
        df1[level_str+'_Standard_Error'] = rmserr1
        df1[level_str+'_CI_Lower'] = lb1
        df1[level_str+'_CI_Upper'] = ub1


        ##Concatenate the two dataframes
        df = pd.concat([df0, df1], axis=0)
        return df1, rmse1
        #return df, rmse0, rmse1

    ##7. Linear Regression
    def linear_regression(self, hseries, fseries, level_str):

        """Linear Regression: In this approach, we estimate the values of the
        slope and intercept of the linear equation Y = m*X + c by fitting the historical values
        which are then used to generate the forward forecast"""
        method_name = 'Linear Regression'
        hcost = hseries[level_str].values
        nf = len(fseries.index.values)
        n = len(hcost)

        ##Create temporary dataframe that contains Variable Time and the Variable Cost
        tdf = pd.DataFrame({'Time': np.arange(1, n + 1), 'Cost': np.array(hcost)})
        lm_s = smf.ols(formula='Cost ~ Time', data=tdf).fit()  ##Fit the historical cost data

        ##Now make predictions on the historical data
        pred = lm_s.predict(tdf)
        err  = pred - hcost
        n  = len(err)
        if n > 2:
            sderr_model = np.sqrt((n - 1) / (n - 2)) * np.std(err)
        else:
            sderr_model = np.std(err)
        ##Compute the forecast accuracy
        rmse, mape = self.forecast_accuracy(hcost, pred)

        ##Make predictions beyond the historical observations
        x_new = pd.DataFrame({'Time': np.arange(n+1, (n + 1) + nf)})
        forecast = lm_s.predict(x_new)

        ##Getting the standard error and confidence intervals around the predicted estimates
        def transform_exog_to_model(fit, exog):
            transform = True
            self1 = fit

            # The following is lifted straight from statsmodels.base.model.Results.predict()
            if transform and hasattr(self1.model, 'formula') and exog is not None:

                exog = dmatrix(self1.model.data.orig_exog.design_info.builder,
                               exog)

                if exog is not None:
                    exog = np.asarray(exog)
                    if exog.ndim == 1 and (self1.model.exog.ndim == 1 or
                                                   self1.model.exog.shape[1] == 1):
                        exog = exog[:, None]
                    exog = np.atleast_2d(exog)  # needed in count model shape[1]

            # end lifted code
            return exog

        ##Use statsmodels.sandbox.regression.prestd.wls_prediction_std to get the
        ##confidence intervals and standard error around our predicted values for
        ##our new data (x_new). In order to do this, we first have to define a
        ##function called transform_exog_to_model that transforms the input x_new
        ##vector into the correct format that is required by wls_prediction_std

        transformed_exog = transform_exog_to_model(lm_s, x_new)
        prstd, iv_l, iv_u = wls_prediction_std(lm_s, transformed_exog)

        t_c = stats.t.ppf(q=0.975, df=n - 2)
        for i in range(nf):
            if forecast[i] < 0.0:
                forecast[i] = min(hcost[-1], np.abs(forecast[i]))
                if forecast[i] == hcost[-1]:
                    iv_l[i] = forecast[i] - sderr_model * t_c
                    if iv_l[i] < 0.0:
                        iv_l[i] = 1e-7
                    iv_u[i] = forecast[i] + sderr_model * t_c
                    prstd[i] = sderr_model
                else:
                    iv_l[i] = forecast[i] - prstd[i] * t_c
                    if iv_l[i] < 0.0:
                        iv_l[i] = 1e-7
                    iv_u[i] = forecast[i] + prstd[i] * t_c
            else:
                if iv_l[i] < 0.0:
                    iv_l[i] = 1e-7

        ##Create a dataframe with the predicitons
        df = pd.DataFrame()
        df = df.append(fseries)
        i = 0
        for index, row in df.iterrows():
            df[level_str].iloc[i] = forecast[i]
            i += 1
        df['Method'] = method_name
        df['Forecast_Identifier'] = '6'
        df[level_str+'_Historical_RMSE'] = rmse
        df[level_str+'_Mape'] = mape
        df[level_str+'_Standard_Error'] = prstd
        df[level_str+'_CI_Lower'] = iv_l
        df[level_str+'_CI_Upper'] = iv_u

        return df, rmse

    ##8. ARIMA (1,1,0): That corresponds to a differenced first order auto-regressive model
    def arima(self, hseries, fseries, level_str):
        """ARIMA: We implement the (1,1,0) arima model that corresponds to a differenced first order
        auto-regressive model AR(1) where in the AR(1) component is used to eliminate or reduce any positive
        autocorrelations in the differenced series.
        """

        method = 'ARIMA'
        nf = len(fseries.index.values)
        n = len(hseries)
        size = int(n * 0.66)
        train, test = hseries[level_str][0:size], hseries[level_str][size:len(hseries)]
        history = [x for x in train]
        test = [x for x in test]
        predictions = list()
        for t in range(len(test)):
            try:
                #model = ARIMA(history, order=(1, 1, 0))
                model = ARIMA(history, order=(1, 1, 0))
                order = (1, 1, 0)
                try:
                    model_fit = model.fit(disp=0)
                    method_name = 'ARIMA ' + str(order)
                    output = model_fit.forecast()
                    yhat = np.abs(output[0][0])
                    if np.isnan(yhat):
                        yhat = test[t]
                    predictions.append(yhat)
                    obs = test[t]
                    history.append(obs)
                except(ValueError, linalgerr):
                    # print("Reverting to a naive forecast")
                    order = (0, 1, 0)
                    method_name = 'ARIMA ' + str(order)
                    yhat = history[-1]
                    predictions.append(yhat)
                    obs = test[t]
            except(ValueError):
                # print("Reverting to a naive forecast")
                order = (0, 1, 0)
                method_name = 'ARIMA ' + str(order)
                yhat = history[-1]
                predictions.append(yhat)
                obs = test[t]

                # print('predicted=%f, expected=%f' % (yhat, obs))

        ##Compute RMSE and MAPE
        rmse, mape = self.forecast_accuracy(test, predictions)
        t_c = stats.t.ppf(q=0.975, df=n - 1)
        ##Generate out-of-sample forecasts
        if order == (1, 1, 0):
            forecast, stderr, conf = model_fit.forecast(steps=nf)
            ##Check for negative forecasts: If there are any negative forecasts, then revert back to the naive
            ##forecast.
            nmask = np.array(forecast) < 0.0
            if len(nmask) > 0:
                forecast = list()
                stderr = list()
                conf = np.zeros((nf, 2))
                for k in range(nf):
                    forecast.append(test[-1])
                    stderr.append(rmse)
                    conf[k][0] = forecast[k] - t_c * stderr[k] * np.sqrt(k + 1)
                    conf[k][1] = forecast[k] + t_c * stderr[k] * np.sqrt(k + 1)
        else:
            # print("Reverting to a naive forecast")
            forecast = list()
            stderr = list()
            conf = np.zeros((nf, 2))
            for k in range(nf):
                forecast.append(test[-1])
                stderr.append(rmse)
                conf[k][0] = forecast[k] - t_c * stderr[k] * np.sqrt(k + 1)
                conf[k][1] = forecast[k] + t_c * stderr[k] * np.sqrt(k + 1)

        ##Sanity check for negative error bounds.
        lb = list()
        ub = list()
        for i in range(nf):
            if conf[i][0] < 0.0:
                conf[i][0] = 0.0
            lb.append(conf[i][0])
            ub.append(conf[i][1])

        ##Create a dataframe with the predicitons
        df = pd.DataFrame()
        df = df.append(fseries)
        df = df.reset_index()
        df = df.drop('index', axis=1)
        i = 0
        #for index, row in df.iterrows():
        #    df.loc[index, level_str] = forecast[i]
        #    i += 1
        df[level_str] = forecast
        df['Method'] = method_name
        df['Forecast_Identifier'] = '7'
        df[level_str+'_Historical_RMSE'] = rmse
        df[level_str+'_Mape'] = mape
        df[level_str+'_Standard_Error'] = stderr
        df[level_str+'_CI_Lower'] = lb
        df[level_str+'_CI_Upper'] = ub

        return df, rmse, method_name

    ##Gamma distribution parameter estimation from percentiles.
    def gamma_parameters(self, x1, p1, x2, p2):

        # Standardize so that x1 < x2 and p1 < p2
        if x1 > x2:
            #(p1, p2) = (p2, p1)
            (x1, x2) = (x2, x1)

        # function to find roots of for gamma distribution parameters
        def objective(alpha):
            return stats.gamma.ppf(p2, alpha) / stats.gamma.ppf(p1, alpha) - x2 / x1

        # The objective function we're wanting to find a root of is decreasing.
        # We need to find an interval over which is goes from positive to negative.
        left = right = 1.0
        while objective(left) < 0.0:
            left /= 2
        while objective(right) > 0.0:
            right *= 2
        try:
            #alpha = bisect(objective, left, right, disp=True)
            alpha = brentq(objective, left, right, disp=True)
            scale = x1 / stats.gamma.ppf(p1, alpha)
            beta = 1.0 / scale  ##Rate parameter is the inverse of the scale parameter
        except(RuntimeError):
            print("Unable to converge")
            alpha = 0.0
            beta  = 0.0
        return (alpha, beta)

    def get_stats(self, alpha, beta, percentile=None):
        """This function is used to calculate the value corresponding to a given percentile.
        It also returns the mean and standard deviation of the distribution.
        The shape and rate parameters are provided as arguments. """
        scale = 1.0 / beta
        if percentile != None:
            per_val = stats.gamma.ppf(percentile, alpha, scale=scale)
            mean = stats.gamma.mean(alpha, scale=scale)
            sd = stats.gamma.std(alpha, scale=scale)
            return (mean, sd, per_val)
        else:
            mean = stats.gamma.mean(alpha, scale=scale)
            sd = stats.gamma.std(alpha, scale=scale)
            return (mean, sd)

    ##Function to generate the probabilities given lower, upper and target cost values
    def get_prob(self, x, x_l, x_u, alpha, beta):
        """This function is used to generate the probability associated with obtaining a certain target cost X,
        from a truncated gamma distribution given the lower and upper bounds for cost, the requested target cost
        and the shape and scale parameters of the gamma distribution function."""
        shape = alpha
        scale = 1.0 / beta
        p_l = stats.gamma.cdf(x_l, shape, scale=scale)
        p_u = stats.gamma.cdf(x_u, shape, scale=scale)
        p_x = stats.gamma.cdf(x, shape, scale=scale)

        return np.abs((p_u - p_x) / (p_u - p_l))

    def forecast_cost(self, hseries, fseries, level, level_str, colsj):
        """This function invokes the suite of forecasting routines to make predictions on the unit cost data."""
        nf = len(fseries.values)
        if len(hseries.values) == 1:
            """Only naive prediction is possible in this case."""
            df, rerr = self.random_walk(hseries, fseries, level_str)
            bvec = np.array(['True' for x in range(nf)])
            jvec = 'Only one data point has been provided for this MPN_ID. Therefore, only the naive method can be used to generate the forecasts. In this case, error metrics such as RMSE and MAPE are undefined.'
            df[level_str+'_Selected'] = bvec
            dfj = df[colsj].iloc[0]
            dfj['Forecasted_Level'] = level
            dfj['Forecasted_Quantity'] = level_str
            dfj['Justification'] = jvec

        elif len(hseries.values) == 2:
            """Except for MA all other approaches possible!!"""
            df0, r0 = self.simple_average(hseries, fseries, level_str)
            df1, r1 = self.random_walk(hseries, fseries, level_str)
            df2, r2 = self.random_walk_drift(hseries, fseries, level_str)
            alpha = 0.65  ##This has been hard coded
            df3, r3 = self.simple_exponential_smoothing(hseries, fseries, level_str, alpha)
            df4, r4 = self.linear_regression(hseries, fseries, level_str)
            df5, r5, mn = self.arima(hseries, fseries, level_str)
            ##Find the min value among all the RMSEs
            rmse = {0: r0, nf: r1, 2 * nf: r2, 3 * nf: r3, 4 * nf: r4, 5 * nf: r5}
            bvec = np.array(['False' for x in range(len(rmse) * nf)])
            # keys = list(rmse.keys())
            ind = min(rmse, key=rmse.get)
            bvec[ind:ind + nf] = 'True'
            ##Concatenate the dataframes into a single data frame
            df = pd.concat([df0, df1, df2, df3, df4, df5])
            jvec = 'The selected method is based on one that gives the minimum RMSE. Additionally with only two data points, the moving average is also unavailable to make predictions.'
            df[level_str+'_Selected'] = bvec
            dfj = df[colsj].iloc[ind]
            dfj['Forecasted_Level'] = level
            dfj['Forecasted_Quantity'] = level_str
            dfj['Justification'] = jvec
        elif len(hseries) == 3:
            """All methods can be used to make predictions. For MA, window size has to be equal to 2"""
            df0, r0 = self.simple_average(hseries, fseries, level_str)
            df1, r1 = self.random_walk(hseries, fseries, level_str)
            df2, r2 = self.random_walk_drift(hseries, fseries, level_str)
            ##Width of the moving average window. Default is set to 3
            window = 2
            df3, r3 = self.moving_average(hseries, fseries, level_str, window)
            alpha = 0.65  ##This has been hard coded
            df4, r4 = self.simple_exponential_smoothing(hseries, fseries, level_str, alpha)
            df5, r5 = self.linear_regression(hseries, fseries, level_str)
            df6, r6, mn = self.arima(hseries, fseries, level_str)
            ##Find the min value among all the RMSEs
            rmse = {0: r0, nf: r1, 2 * nf: r2, 3 * nf: r3, 4 * nf: r4, 5 * nf: r5, 6 * nf: r6}
            bvec = np.array(['False' for x in range(len(rmse) * nf)])
            # keys = list(rmse.keys())
            ind = min(rmse, key=rmse.get)
            bvec[ind:ind + nf] = 'True'
            ##Concatenate the dataframes into a single data frame
            df = pd.concat([df0, df1, df2, df3, df4, df5, df6])
            # display(df0)
            jvec = 'The selected method is based on one that gives the minimum RMSE. Additionally with three data points, only the moving average with window width set equal to 2 is available to make predictions.'
            df[level_str+'_Selected'] = bvec
            dfj = df[colsj].iloc[ind]
            dfj['Forecasted_Level'] = level
            dfj['Forecasted_Quantity'] = level_str
            dfj['Justification'] = jvec
        else:  ##For npoints > 3
            """All methods can be used to make predictions."""
            df0, r0 = self.simple_average(hseries, fseries, level_str)
            df1, r1 = self.random_walk(hseries, fseries, level_str)
            df2, r2 = self.random_walk_drift(hseries, fseries, level_str)
            ##Width of the moving average window. Default is set to 3
            window = 3
            df3, r3 = self.moving_average(hseries, fseries, level_str, window)
            alpha = 0.65  ##This has been hard coded
            df4, r4 = self.simple_exponential_smoothing(hseries, fseries, level_str, alpha)
            df5, r5 = self.linear_regression(hseries, fseries, level_str)
            df6, r6, mn = self.arima(hseries, fseries, level_str)
            ##Find the min value among all the RMSEs
            rmse = {0: r0, nf: r1, 2 * nf: r2, 3 * nf: r3, 4 * nf: r4, 5 * nf: r5, 6 * nf: r6}
            bvec = np.array(['False' for x in range(len(rmse) * nf)])
            # keys = list(rmse.keys())
            ind = min(rmse, key=rmse.get)
            bvec[ind:ind + nf] = 'True'
            ##Concatenate the dataframes into a single data frame
            df = pd.concat([df0, df1, df2, df3, df4, df5, df6])
            # display(df0)
            jvec = 'The selected method is based on one that gives the minimum RMSE. Consequently, the selected methods also has the lowest MAPE.'
            df[level_str+'_Selected'] = bvec
            dfj = df[colsj].iloc[ind]
            dfj['Forecasted_Level'] = level
            dfj['Forecasted_Quantity'] = level_str
            dfj['Justification'] = jvec

        ##Convert dfj to a dataframe
        dfj = dfj.to_frame().transpose()
        return df, dfj