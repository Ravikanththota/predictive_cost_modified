testQuery = "select version()"

#customerData = """select * from customer_c"""

#partClassificationData = """select * from part_c"""

customerData = """SELECT ci.CUSTOMER_NAME AS 'Customer_ID',
    cpn.cpn AS 'Customer_Part_Number',
    mpn.mpn AS 'Manufacturer_Part_Number',
    md.MANUFACTURER_NAME AS 'Manufacturer',
    cod.CM_ODM_NAME AS 'CM_or_ODM',
    i.CALENDAR_DATE AS 'Time_Period',
    CASE
       WHEN MIN(NULLIF(MPN_COST, 0)) is NULL THEN 'NaN' 
        ELSE MIN(NULLIF(MPN_COST, 0))
    END AS 'MPN_Unit_Cost',
    q.Quantity AS 'Quantity',
    (i.AWARDED_MPN_SPLIT / 100.00) AS 'MPN_Split'
   
FROM
    item_fact i
        INNER JOIN
    cpn_dimension cpn ON cpn.cpn_id = i.cpn_id
        INNER JOIN
    mpn_dimension mpn ON mpn.MPN_ID = i.MPN_ID
        INNER JOIN
    manufacturer_dimension md ON md.manufacturer_id = i.manufacturer_id
        INNER JOIN
    cm_odm_dimension cod ON cod.CM_ODM_ID = i.CM_ODM_ID
    inner join  (select cpn_id,CM_ODM_ID,CALENDAR_DATE,sum(CPN_ACTUAL_DEMAND) as 'Quantity' from item_fact 
   group by cpn_id,CM_ODM_ID,CALENDAR_DATE) q on q.cpn_id=i.CPN_ID
   and q.cm_odm_id=i.CM_ODM_ID and q.calendar_date=i.calendar_date
           CROSS JOIN
    customer_info ci
    group by i.cpn_id,i.MPN_ID,i.manufacturer_id,i.CM_ODM_ID,i.CALENDAR_DATE
ORDER BY i.cpn_id,i.MPN_ID,i.manufacturer_id,i.CM_ODM_ID,i.CALENDAR_DATE"""

partClassificationData = """SELECT DISTINCT
    cpn.cpn AS 'Customer_Part_Number',
    mpn.mpn AS 'Manufacturer_Part_Number',
    md.MANUFACTURER_NAME AS 'Manufacturer',
    cpn.COMMODITY_NAME AS 'Commodity',
    cpn.SUB_COMMODITY_NAME AS 'SubCommodity',
    cdr.MASTER_COMMODITY AS 'LevaData_SubCommodity'
FROM
    item_fact i
        INNER JOIN
    cpn_dimension cpn ON cpn.cpn_id = i.cpn_id
        INNER JOIN
    mpn_dimension mpn ON mpn.MPN_ID = i.MPN_ID
        INNER JOIN
    manufacturer_dimension md ON md.manufacturer_id = i.manufacturer_id
        INNER JOIN
    commodity_dictionary_rawdata cdr ON cdr.VENDOR_COMMODITY = cpn.SUB_COMMODITY_NAME;"""

systemInformation = " select current_date() as 'Current_Date','QUARTERLY' as 'Time_Period_Size';"

